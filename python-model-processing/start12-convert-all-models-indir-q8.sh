#!/bin/bash

bdir="/Users/alexanderkalinovsky/Workspace/tensorflow-1.0.0-sdk/bazel-bin"
wdir="00_model_processing"

runpy="run01_keras2tf_v2.py"

echo "*** directory with models is : [${wdir}]"

find ${wdir} -maxdepth 1 -name '*.h5' | while read ll
do
    echo "::processing Model [${ll}]"
    bn=`basename ${ll}`
    odir="${wdir}/${bn}-dir"
    mkdir -p "${odir}"
    cp ${ll} ${odir}/
    python ${runpy} ${odir} ${bdir}
done
