mkdir -p ~/Workspace_TF
cd ~/Workspace_TF

# (1) Устанавливаем python-версию TF & keras (не паримся с GPU - ставим CPU-версию)
pip install tensorflow -U
pip install keras -U


Скачиваем TF:
wget https://github.com/tensorflow/tensorflow/archive/v1.1.0-rc2.zip



unzip tensorflow-1.1.0.zip


cd tensorflow-1.1.0
./configure

bazel build --config=opt //tensorflow/tools/pip_package:build_pip_package


bazel build tensorflow/python/tools:freeze_graph
bazel build tensorflow/python/tools:optimize_for_inference
bazel build tensorflow/tools/quantization:quantize_graph
