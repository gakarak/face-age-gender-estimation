package org.tensorflow.demo;

import android.content.res.AssetManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alexanderkalinovsky on 5/29/17.
 */

public class ModelInpOut {
    // FIXME: supported only graph with one-Input
    public DataInp dataInp;
    public HashMap<String, DataOut> mapOut;

    private ModelInpOut() {}

    public static ModelInpOut build(String modelConfigFileJSON, AssetManager manager) throws IOException, JSONException {
        ModelInpOut ret = new ModelInpOut();
        String strCfgJSON = _getAssetJSONFile(modelConfigFileJSON, manager);
        JSONObject obj = new JSONObject(strCfgJSON);
        // Inputs:
        JSONArray arrInp = obj.getJSONArray("inps");
        JSONObject objInp = arrInp.getJSONObject(0);
        ret.dataInp = ModelInpOut.buildInpFromJsonObj(objInp);
        // Outputs:
        JSONArray arrOut = obj.getJSONArray("outs");
        ret.mapOut = new HashMap<>();
        for(int i=0; i<arrOut.length(); i++) {
            DataOut tout = ModelInpOut.buildOutFromJsonObj(arrOut.getJSONObject(i));
            ret.mapOut.put(tout.name, tout);
        }
        return ret;
    }

    public String[] outputNames() {
        ArrayList<String> ret = new ArrayList<>();
        int numOut = mapOut.size();
        for(Map.Entry<String, DataOut> kv: mapOut.entrySet()) {
            ret.add(kv.getValue().name);
        }
        return ret.toArray(new String[numOut]);
    }

    ///////////////////////////////////
    public static String _getAssetJSONFile(String filename, AssetManager manager) throws IOException {
        InputStream file = manager.open(filename);
        byte[] formArray = new byte[file.available()];
        file.read(formArray);
        file.close();
        return new String(formArray);
    }

    public static DataOut buildOutFromJsonObj(JSONObject objOut) throws JSONException {
        String pname = objOut.getString("name");
        String ptype = objOut.getString("type");
        ArrayList<Integer> pshape = new ArrayList<>();
        JSONArray arrShape = objOut.getJSONArray("shape");
        for(int i=0; i<arrShape.length(); i++) {
            pshape.add(arrShape.getInt(i));
        }
        ArrayList<String> catNames = null;
        if("category".equals(ptype)) {
            catNames = new ArrayList<>();
            JSONArray arrLbl = objOut.getJSONArray("lbl");
            for(int i=0; i<arrLbl.length(); i++) {
                catNames.add(arrLbl.getString(i));
            }
        }
        DataOut ret = new DataOut(pname, pshape, ptype, catNames);
        return ret;
    }

    public static DataInp buildInpFromJsonObj(JSONObject objInp) throws JSONException {
        String pname = objInp.getString("name");
        float pmean = 127.5f;
        float pstd  = 127.5f;
        ArrayList<Integer> pshape = new ArrayList<>();
        JSONArray arrInpShape = objInp.getJSONArray("shape");
        if(objInp.has("inpMean")) {
            pmean = (float)objInp.getDouble("inpMean");
        }
        if(objInp.has("inpStd")) {
            pstd = (float)objInp.getDouble("inpStd");
        }
        for(int i=0; i<arrInpShape.length(); i++) {
            pshape.add(arrInpShape.getInt(i));
        }
        DataInp ret = new DataInp(pname, pshape, pmean, pstd);
        return ret;
    }

    ///////////////////////////////////
    public static class ResultValue {
        public float result;
        public float confidence;
        private ResultValue() {}
        public static ResultValue build(float presult, float pconfidence) {
            ResultValue ret = new ResultValue();
            ret.result = presult;
            ret.confidence = pconfidence;
            return ret;
        }
    }

    public static class DataInp {
        public String name;
        public ArrayList<Integer> shape;
        public float vmean;
        public float vstd;
        public int[] buffIntValues;
        public float[] buffInpValues;

        public DataInp(String pname, ArrayList<Integer> pshape, float pmean, float pstd) {
            name = pname;
            shape = pshape;
            vmean = pmean;
            vstd = pstd;
            int imgR = pshape.get(0);
            int imgC = pshape.get(1);
            int ndim = pshape.get(2);
            buffIntValues = new int[imgR * imgC];
            buffInpValues = new float[imgR * imgC * ndim];
        }
    }

    public static class DataOut {
        public String name;
        public ArrayList<Integer> shape;
        public float[] buffOutValues;
        public String type;
        public ArrayList<String> catNames;

        DataOut(String pname, ArrayList<Integer> pshape, String ptype, ArrayList<String> pcatNames) {
            name = pname;
            shape = pshape;
            type = ptype;
            catNames = pcatNames;
            //FIXME: only one-dimensional output currecntly supported
            int tsize = size();
            buffOutValues = new float[tsize];
        }

        public ResultValue getResult() {
            if(isCategorical()) {
                int idxMax = 0;
                float valMax = buffOutValues[0];
                for (int i=1; i<buffOutValues.length; i++) {
                    if (buffOutValues[i]>valMax) {
                        valMax = buffOutValues[i];
                        idxMax = i;
                    }
                }
                return ResultValue.build(idxMax, valMax);
            } else if (isScalar()) {
                return ResultValue.build(buffOutValues[0], 1.0f);
            } else {
                return null;
            }
        }

        public String getResultStr(ResultValue pres) {
            ResultValue res = pres;
            if(pres==null) {
                res = getResult();
            }
            if(isCategorical()) {
                return catNames.get((int)res.result);
            } else if (isScalar()) {
                return String.format("%0.2f", res.result);
            } else {
                return "Unknown";
            }
            /*
            if(isCategorical()) {
                int idxMax = 0;
                float valMax = buffOutValues[0];
                for (int i=1; i<buffOutValues.length; i++) {
                    if (buffOutValues[i]>valMax) {
                        valMax = buffOutValues[i];
                        idxMax = i;
                    }
                }
                return catNames.get(idxMax);
            } else if (isScalar()) {
                return String.format("%0.2f", buffOutValues[0]);
            } else {
                return "Unknown";
            }
            */
        }

        public int size() {
            //FIXME: only one-dimensional output currecntly supported
            return shape.get(0);
        }

        boolean isCategorical() {
            return "category".equals(this.type);
        }

        boolean isScalar() {
            return "scalar".equals(this.type);
        }

    }
}
