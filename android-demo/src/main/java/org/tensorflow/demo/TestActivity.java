package org.tensorflow.demo;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class TestActivity extends Activity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    public static final String TAG = "TAG_TestActivity";

    private static String MODEL_DIR = "models";

    private Button btnPush = null;
    private Button btnCancel = null;
    private Button btnRecognize = null;
    private Button btnClear = null;
    private TextView textView = null;
    private Spinner spinModels = null;
    private ArrayList<String> modelList = null;
    ArrayAdapter<CharSequence> spinModelsAdapter = null;

    TestTFModel tfModel = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        btnPush = (Button) findViewById(R.id.tst_btnPush);
        btnCancel = (Button) findViewById(R.id.tst_btnLoadModel);
        btnRecognize = (Button) findViewById(R.id.tst_btnRecognize);
        btnClear = (Button) findViewById(R.id.tst_btnClear);
        spinModels = (Spinner) findViewById(R.id.spin_models);
        textView = (TextView) findViewById(R.id.tst_textView);

        btnPush.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnRecognize.setOnClickListener(this);
        btnClear.setOnClickListener(this);

        _updateModels();

        spinModels.setOnItemSelectedListener(this);
    }

    private void _loadSelectedModel() {
        int modelPos = spinModels.getSelectedItemPosition();
        String modelName = modelList.get(modelPos);
        tfModel = TestTFModel.create(getAssets(), MODEL_DIR + "/" + modelName);
    }

    private void _updateModels() {
        Log.d(TAG, ":: btnPush()");
        modelList = new ArrayList<>();
        String [] list = null;
        try {
            list = getAssets().list(MODEL_DIR);
            for(String s : list) {
                if(s.endsWith(".pb")) {
                    modelList.add(s);
                }
            }
            spinModelsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
            spinModelsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinModels.setAdapter(spinModelsAdapter);
            for(String s : modelList) {
                spinModelsAdapter.add(s);
            }
            Log.d(TAG, "FUCK");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tst_btnLoadModel:
                int cpos = spinModels.getSelectedItemPosition();
                String modelName = modelList.get(cpos);
                Log.d(TAG, ":: btnTest() : " + "[" + cpos + "] : " + spinModels.getSelectedItem().toString());
                _loadSelectedModel();
                break;
            case R.id.tst_btnPush:
                Log.d(TAG, ":: btnPush()");
                _updateModels();
                break;
            case R.id.tst_btnRecognize:
                _loadSelectedModel();
                _testRecognition();
                Log.d(TAG, ":: btnRecognize()");
                break;
            case R.id.tst_btnClear:
                textView.setText("");
                Log.d(TAG, ":: btnClear()");
                break;
        }
    }

    private Bitmap _readBitmapFromAsset(String fimg) {
        AssetManager assetManager = getAssets();
        InputStream istr = null;
        try {
            istr = assetManager.open(fimg);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(istr);
        return bitmap;
    }

    private void _testRecognition() {
        final String fimg = "img_test_01.jpg";
        final Bitmap bitmap = _readBitmapFromAsset(fimg);
        if(tfModel==null) {
            _loadSelectedModel();
        }
        List<Classifier.Recognition> ret = tfModel.recognizeImage(bitmap);
        for(Classifier.Recognition r : ret) {
            textView.append(r.toString() + "\n");
        }
        textView.append("-------\n\n");
        Log.d(TAG, "::_testRecognition() : " + ret.toString());
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
