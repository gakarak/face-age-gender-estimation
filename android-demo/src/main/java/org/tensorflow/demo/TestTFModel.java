package org.tensorflow.demo;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.os.Trace;

import org.json.JSONException;
import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by alexanderkalinovsky on 5/26/17.
 */

public class TestTFModel {
    private static final String TAG = "TestTFModel";
    private TensorFlowInferenceInterface inferenceInterface = null;
    private ModelInpOut inpOut = null;

//    String inpName = null;
//    String outName = null;

//    private int[] intValues;
//    private float[] floatValues;
//    private float[] outputs;
    private String[] outputNames;

//    private int     inputSize;
//    private int     imageMean;
//    private float   imageStd;

    private TestTFModel() {

    }

    public static TestTFModel create(
            AssetManager assetManager,
            String modelFilename) {
        TestTFModel c = new TestTFModel();
//        c.inpName = inputName;
//        c.outName = outputName;
        String modelConfigFileJSON = modelFilename.substring(0, modelFilename.lastIndexOf('.')) + ".json";
        try {
            c.inpOut = ModelInpOut.build(modelConfigFileJSON, assetManager);
            c.inferenceInterface = new TensorFlowInferenceInterface(assetManager, modelFilename);
//        final Operation operation = c.inferenceInterface.graphOperation(outputName);
//        final int numClasses = (int) operation.output(0).shape().size(1);
//        Log.i(TAG, "Output layer size is " + numClasses);

//            c.inputSize = inputSize;
//            c.imageMean = imageMean;
//            c.imageStd = imageStd;

            // Pre-allocate buffers.
            c.outputNames = c.inpOut.outputNames();
//            c.intValues   = new int[inputSize * inputSize];
//            c.floatValues = new float[inputSize * inputSize * 3];
//        c.outputs = new float[numClasses];
            return c;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Classifier.Recognition> recognizeImage(final Bitmap bitmap) {
        Trace.beginSection("preprocessBitmap");
        bitmap.getPixels(inpOut.dataInp.buffIntValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        float vmean = inpOut.dataInp.vmean;
        float vstd  = inpOut.dataInp.vstd;
        for (int i = 0; i < inpOut.dataInp.buffIntValues.length; ++i) {
            final int val = inpOut.dataInp.buffIntValues[i];
            inpOut.dataInp.buffInpValues[i * 3 + 0] = (((val >> 16) & 0xFF) - vmean) / vstd;
            inpOut.dataInp.buffInpValues[i * 3 + 1] = (((val >> 8) & 0xFF) - vmean) / vstd;
            inpOut.dataInp.buffInpValues[i * 3 + 2] = ((val & 0xFF) - vmean) / vstd;
        }
        Trace.endSection();

        // Copy the input data into TensorFlow.
        Trace.beginSection("feed");
        ArrayList<Integer> inpShape = inpOut.dataInp.shape;
        inferenceInterface.feed(inpOut.dataInp.name, inpOut.dataInp.buffInpValues, 1,
                inpShape.get(0), inpShape.get(1), inpShape.get(2));
        Trace.endSection();

        // Run the inference call.
        Trace.beginSection("run");
        inferenceInterface.run(outputNames, false);
        Trace.endSection();

        ArrayList<Classifier.Recognition> ret = new ArrayList<>();
        int numOut = inpOut.mapOut.size();
        Trace.beginSection("fetch");
        for(Map.Entry<String, ModelInpOut.DataOut> kv: inpOut.mapOut.entrySet()) {
//            ret.add(kv.getValue().name);
            String outputName = kv.getKey();
            inferenceInterface.fetch(outputName, kv.getValue().buffOutValues);

            ModelInpOut.ResultValue resultValue = kv.getValue().getResult();
            String resultStr = kv.getValue().getResultStr(resultValue);
            ret.add(new Classifier.Recognition(kv.getKey(), resultStr, resultValue.confidence, null));
        }
        Trace.endSection();
        return ret;
    }
}
