
package org.tensorflow.demo;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.os.Trace;
import android.util.Log;

import org.json.JSONException;
import org.tensorflow.Operation;
import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * A classifier specialized to label images using TensorFlow.
 */
public class TensorFlowFaceAnalyser implements Classifier {
    private static final String TAG = "TensorFlowFaceAnalyser";

    private boolean logStats = false;

    private TensorFlowInferenceInterface inferenceInterface;
    public ModelInpOut inpOut = null;
    private String[] outputNames = null;

    private TensorFlowFaceAnalyser() {
    }

    /**
     * @param assetManager  The asset manager to be used to load assets.
     * @param modelFilename The filepath of the model GraphDef protocol buffer in JSON format
     * @throws IOException
     */
    public static Classifier create(
            AssetManager assetManager,
            String modelFilename) {
        TensorFlowFaceAnalyser c = new TensorFlowFaceAnalyser();
        c.inferenceInterface = new TensorFlowInferenceInterface(assetManager, modelFilename);
        String modelConfigFileJSON = modelFilename.substring(0, modelFilename.lastIndexOf('.')) + ".json";
        try {
            c.inpOut = ModelInpOut.build(modelConfigFileJSON, assetManager);
            c.inferenceInterface = new TensorFlowInferenceInterface(assetManager, modelFilename);
            c.outputNames = c.inpOut.outputNames();
            return c;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Recognition> recognizeImage(final Bitmap bitmap) {
        Trace.beginSection("preprocessBitmap");
        bitmap.getPixels(inpOut.dataInp.buffIntValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
        float vmean = inpOut.dataInp.vmean;
        float vstd  = inpOut.dataInp.vstd;
        for (int i = 0; i < inpOut.dataInp.buffIntValues.length; ++i) {
            final int val = inpOut.dataInp.buffIntValues[i];
            inpOut.dataInp.buffInpValues[i * 3 + 0] = (((val >> 16) & 0xFF) - vmean) / vstd;
            inpOut.dataInp.buffInpValues[i * 3 + 1] = (((val >> 8) & 0xFF) - vmean) / vstd;
            inpOut.dataInp.buffInpValues[i * 3 + 2] = ((val & 0xFF) - vmean) / vstd;
        }
        Trace.endSection();

        Trace.beginSection("feed");
        ArrayList<Integer> inpShape = inpOut.dataInp.shape;
        inferenceInterface.feed(inpOut.dataInp.name, inpOut.dataInp.buffInpValues, 1, inpShape.get(0), inpShape.get(1), inpShape.get(2));
        Trace.endSection();

        Trace.beginSection("run");
        inferenceInterface.run(outputNames, logStats);
        Trace.endSection();

        ArrayList<Classifier.Recognition> ret = new ArrayList<>();
        int numOut = inpOut.mapOut.size();
        Trace.beginSection("fetch");
        float meanConfidence = 0.f;
        String strSummary = "";
        String strId = "*";
        for(Map.Entry<String, ModelInpOut.DataOut> kv: inpOut.mapOut.entrySet()) {
//            ret.add(kv.getValue().name);
            String outputName = kv.getKey();
            inferenceInterface.fetch(outputName, kv.getValue().buffOutValues);
            ModelInpOut.ResultValue resultValue = kv.getValue().getResult();
            String resultStr = kv.getValue().getResultStr(resultValue);
            if(numOut>0) {
                strSummary += kv.getKey().substring(0,1) + ": " + resultStr + "\n";
            } else {
                strSummary = resultStr;
                strId = kv.getKey();
            }
            meanConfidence += resultValue.confidence;
        }
        meanConfidence /= numOut;
        ret.add(new Classifier.Recognition(strId, strSummary, meanConfidence, null));
        Trace.endSection();
        return ret;
    }

    @Override
    public void enableStatLogging(boolean logStats) {
        this.logStats = logStats;
    }

    @Override
    public String getStatString() {
        return inferenceInterface.getStatString();
    }

    @Override
    public void close() {
        inferenceInterface.close();
    }




}
