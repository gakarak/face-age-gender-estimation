/*
 * Copyright 2016 The TensorFlow Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.tensorflow.demo;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.media.FaceDetector;
import android.media.Image;
import android.media.Image.Plane;
import android.media.ImageReader;
import android.media.ImageReader.OnImageAvailableListener;
import android.os.SystemClock;
import android.os.Trace;
import android.util.Log;
import android.util.Size;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import org.tensorflow.demo.OverlayView.DrawCallback;
import org.tensorflow.demo.env.BorderedText;
import org.tensorflow.demo.env.ImageUtils;
import org.tensorflow.demo.env.Logger;
import org.tensorflow.demo.tracking.MultiBoxTracker;

/**
 * An activity that uses a TensorFlowMultiBoxDetector and ObjectTracker to detect and then track
 * objects.
 */
public class FaceAnalysisActivity extends CameraActivity implements View.OnClickListener, OnImageAvailableListener, AdapterView.OnItemSelectedListener {
    public static final String TAG = "TAG_FaceAnalysis";
    private static final Logger LOGGER = new Logger();

    // Configuration values for the prepackaged multibox model.
  /*
  private static final int MB_INPUT_SIZE = 224;
  private static final int MB_IMAGE_MEAN = 128;
  private static final float MB_IMAGE_STD = 128;
  private static final String MB_INPUT_NAME = "ResizeBilinear";
  private static final String MB_OUTPUT_LOCATIONS_NAME = "output_locations/Reshape";
  private static final String MB_OUTPUT_SCORES_NAME = "output_scores/Reshape";
  private static final String MB_MODEL_FILE = "file:///android_asset/multibox_model.pb";
  private static final String MB_LOCATION_FILE = "file:///android_asset/multibox_location_priors.txt";
  */
//  private static final String MODEL_FILE = "file:///android_asset/models/test_cnn_model_mout_gender-age-race.h5-final.pb";
    // (1) Gender
    /*
    private static final String MODEL_FILE = "file:///android_asset/models/test_cnn_model_gender.h5-final.pb";
    private static final String MODEL_NAME_INP = "input_2";
    private static final String MODEL_NAME_OUT = "dense_4/Softmax";
    //  private static final int CROP_SIZE = USE_YOLO ? YOLO_INPUT_SIZE : MB_INPUT_SIZE;
    private static final int CROP_SIZE = 128;
    */
    // Minimum detection confidence to track a detection.
//  private static final float MINIMUM_CONFIDENCE = USE_YOLO ? 0.25f : 0.1f;
//  private static final boolean MAINTAIN_ASPECT = USE_YOLO;
    private static final boolean MAINTAIN_ASPECT = false;
    private static final float MINIMUM_CONFIDENCE = 0.1f;

    // Configuration values for tiny-yolo-voc. Note that the graph is not included with TensorFlow and
    // must be manually placed in the assets/ directory by the user.
    // Graphs and models downloaded from http://pjreddie.com/darknet/yolo/ may be converted e.g. via
    // DarkFlow (https://github.com/thtrieu/darkflow). Sample command:
    // ./flow --model cfg/tiny-yolo-voc.cfg --load bin/tiny-yolo-voc.weights --savepb --verbalise=True
  /*
  private static final String YOLO_MODEL_FILE = "file:///android_asset/graph-tiny-yolo-voc.pb";
  private static final int YOLO_INPUT_SIZE = 416;
  private static final String YOLO_INPUT_NAME = "input";
  private static final String YOLO_OUTPUT_NAMES = "output";
  private static final int YOLO_BLOCK_SIZE = 32;
  */
    private static final Size DESIRED_PREVIEW_SIZE = new Size(640, 480);
    private static final boolean SAVE_PREVIEW_BITMAP = false;
    private static final float TEXT_SIZE_DIP = 10;

    // Default to the included multibox model.
//  private static final boolean USE_YOLO = false;
    private static String MODEL_DIR = "models";
    ArrayAdapter<CharSequence> spinModelsAdapter = null;
    OverlayView trackingOverlay;
    private Button btnLoadModel = null;
    private Spinner spinModels = null;
    private ArrayList<String> modelList = null;
    private Integer sensorOrientation;
    private TensorFlowFaceAnalyser faceAnalyzer;
    private int previewWidth = 0;
    private int previewHeight = 0;
    private byte[][] yuvBytes;
    private int[] rgbBytes = null;
    private Bitmap rgbFrameBitmap = null;
    private Bitmap croppedBitmap = null;
    private boolean computing = false;
    private long timestamp = 0;
    private Matrix frameToCropTransform;
    private Matrix frameToNormalViewTransform;
    private Matrix cropToFrameTransform;
    private Bitmap cropCopyBitmap;
    private MultiBoxTracker tracker;
    private byte[] luminance;
    private BorderedText borderedText;
    private long lastProcessingTimeMs;

    @Override
    public void onPreviewSizeChosen(final Size size, final int rotation) {
        final float textSizePx =
                TypedValue.applyDimension(
                        TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, getResources().getDisplayMetrics());
        borderedText = new BorderedText(textSizePx);
        borderedText.setTypeface(Typeface.MONOSPACE);

        tracker = new MultiBoxTracker(this);

        faceAnalyzer = null;

        previewWidth = size.getWidth();
        previewHeight = size.getHeight();

        final Display display = getWindowManager().getDefaultDisplay();
        final int screenOrientation = display.getRotation();

        LOGGER.i("Sensor orientation: %d, Screen orientation: %d", rotation, screenOrientation);

//    sensorOrientation = screenOrientation;
        sensorOrientation = rotation + screenOrientation;

        LOGGER.i("Initializing at size %dx%d", previewWidth, previewHeight);
        rgbBytes = new int[previewWidth * previewHeight];
        rgbFrameBitmap = Bitmap.createBitmap(previewWidth, previewHeight, Config.ARGB_8888);
//        croppedBitmap = Bitmap.createBitmap(CROP_SIZE, CROP_SIZE, Config.ARGB_8888);
        croppedBitmap = Bitmap.createBitmap(previewHeight, previewWidth, Config.ARGB_8888);

        frameToCropTransform =
                ImageUtils.getTransformationMatrix(
                        previewWidth, previewHeight,
//                        CROP_SIZE, CROP_SIZE,
                        previewHeight, previewWidth,
                        sensorOrientation, MAINTAIN_ASPECT);

        frameToNormalViewTransform = ImageUtils.getTransformationMatrix(
                previewWidth, previewHeight,
                previewWidth, previewHeight,
                sensorOrientation, MAINTAIN_ASPECT);

        cropToFrameTransform = new Matrix();
        frameToCropTransform.invert(cropToFrameTransform);
        yuvBytes = new byte[3][];

        trackingOverlay = (OverlayView) findViewById(R.id.tracking_overlay);
        trackingOverlay.addCallback(
                new DrawCallback() {
                    @Override
                    public void drawCallback(final Canvas canvas) {
                        tracker.draw(canvas);
                        if (isDebug()) {
                            tracker.drawDebug(canvas);
                        }
                    }
                });
        //
        spinModels = (Spinner) findViewById(R.id.spin_models);
        btnLoadModel = (Button) findViewById(R.id.btn_loadModel);
        final Button btnSwitchCamera = (Button) findViewById(R.id.btn_goToTheNextCamera);
        _updateModels();
        spinModels.setOnItemSelectedListener(this);
        btnLoadModel.setOnClickListener(this);
        btnSwitchCamera.setOnClickListener(this);
        //
        addCallback(
                new DrawCallback() {
                    @Override
                    public void drawCallback(final Canvas canvas) {
                        if (!isDebug()) {
                            return;
                        }
                        final Bitmap copy = cropCopyBitmap;
                        if (copy == null) {
                            return;
                        }

                        final int backgroundColor = Color.argb(100, 0, 0, 0);
                        canvas.drawColor(backgroundColor);

                        final Matrix matrix = new Matrix();
                        final float scaleFactor = 2;
                        matrix.postScale(scaleFactor, scaleFactor);
                        matrix.postTranslate(
                                canvas.getWidth() - copy.getWidth() * scaleFactor,
                                canvas.getHeight() - copy.getHeight() * scaleFactor);
                        canvas.drawBitmap(copy, matrix, new Paint());

                        final Vector<String> lines = new Vector<String>();
                        if (faceAnalyzer != null) {
                            final String statString = faceAnalyzer.getStatString();
                            final String[] statLines = statString.split("\n");
                            for (final String line : statLines) {
                                lines.add(line);
                            }
                        }
                        lines.add("");
                        lines.add("Frame: " + previewWidth + "x" + previewHeight);
                        lines.add("Crop: " + copy.getWidth() + "x" + copy.getHeight());
                        lines.add("View: " + canvas.getWidth() + "x" + canvas.getHeight());
                        lines.add("Rotation: " + sensorOrientation);
                        lines.add("Inference time: " + lastProcessingTimeMs + "ms");

                        borderedText.drawLines(canvas, 10, canvas.getHeight() - 10, lines);
                    }
                });
    }

    private Bitmap _cropBitmapByBBox(final Bitmap b, PointF p00, PointF p11, int targetWidth, int targetHeight) {
        RectF rect = new RectF(p00.x, p00.y, p11.x, p11.y);
        Matrix tmat = new Matrix();
//    int targetWidth  = 128;
//    int targetHeight = 128;
        float sx = Math.abs(targetWidth / rect.width());
        float sy = Math.abs(targetHeight / rect.height());
        Bitmap ret = Bitmap.createBitmap(targetWidth, targetHeight, Config.ARGB_8888);
        final Canvas c = new Canvas(ret);
        c.scale(Math.abs(sx), Math.abs(sy));
        c.translate(-p00.x, -p00.y);
        c.drawBitmap(b, tmat, null);
        return ret;
    }

    @Override
    public void onImageAvailable(final ImageReader reader) {
        Image image = null;

        ++timestamp;
        final long currTimestamp = timestamp;

        try {
            image = reader.acquireLatestImage();

            if (image == null) {
                return;
            }

            Trace.beginSection("imageAvailable");

            final Plane[] planes = image.getPlanes();
            fillBytes(planes, yuvBytes);

      /**/
            tracker.onFrame(
                    previewWidth,
                    previewHeight,
                    planes[0].getRowStride(),
                    sensorOrientation,
                    yuvBytes[0],
                    timestamp);
      /**/
            trackingOverlay.postInvalidate();

            // No mutex needed as this method is not reentrant.
            if (computing) {
                image.close();
                return;
            }
            computing = true;

            final int yRowStride = planes[0].getRowStride();
            final int uvRowStride = planes[1].getRowStride();
            final int uvPixelStride = planes[1].getPixelStride();
            ImageUtils.convertYUV420ToARGB8888(
                    yuvBytes[0],
                    yuvBytes[1],
                    yuvBytes[2],
                    previewWidth,
                    previewHeight,
                    yRowStride,
                    uvRowStride,
                    uvPixelStride,
                    rgbBytes);

            image.close();
        } catch (final Exception e) {
            if (image != null) {
                image.close();
            }
            LOGGER.e(e, "Exception!");
            Trace.endSection();
            return;
        }

        rgbFrameBitmap.setPixels(rgbBytes, 0, previewWidth, 0, 0, previewWidth, previewHeight);
        final Canvas canvas = new Canvas(croppedBitmap);
        canvas.drawBitmap(rgbFrameBitmap, frameToCropTransform, null);
//    canvas.drawBitmap(rgbFrameBitmap, new Matrix(), null);

        // For examining the actual TF input.
    /*
    if (SAVE_PREVIEW_BITMAP) {
      ImageUtils.saveBitmap(croppedBitmap);
    }
    */
//        ImageUtils.saveBitmap(croppedBitmap);

        if (luminance == null) {
            luminance = new byte[yuvBytes[0].length];
        }
        System.arraycopy(yuvBytes[0], 0, luminance, 0, luminance.length);
        //
        final List<Classifier.Recognition> results = new ArrayList<Classifier.Recognition>();
//        if (faceAnalyzer != null) {
            BitmapFactory.Options BitmapFactoryOptionsbfo = new BitmapFactory.Options();
            BitmapFactoryOptionsbfo.inPreferredConfig = Bitmap.Config.RGB_565;
//            Bitmap bitmap = rgbFrameBitmap.copy(Bitmap.Config.RGB_565, true);
            Bitmap bitmap = croppedBitmap.copy(Bitmap.Config.RGB_565, true);
            int numberOfFaces = 5;
            int imageWidth = bitmap.getWidth();
            int imageHeight = bitmap.getHeight();
            FaceDetector.Face[] myFace = new FaceDetector.Face[numberOfFaces];
            FaceDetector myFaceDetect = new FaceDetector(imageWidth, imageHeight, numberOfFaces);
            int numberOfFaceDetected = myFaceDetect.findFaces(bitmap, myFace);
            for (int ii = 0; ii < numberOfFaceDetected; ii++) {
                PointF p0 = new PointF();
                float dstEye = myFace[ii].eyesDistance();
                myFace[ii].getMidPoint(p0);
//                canvas.drawLine(p0.x - dstEye/2, p0.y, p0.x + dstEye/2, p0.y, new Paint());
                PointF p00 = new PointF(p0.x - 1.4f * dstEye, p0.y - 1.15f * dstEye);
                PointF p11 = new PointF(p0.x + 1.4f * dstEye, p0.y + 1.6f * dstEye);
                RectF rect = new RectF(p00.x, p00.y, p11.x, p11.y);
                if (isDebug()) {
//                    cropCopyBitmap = _cropBitmapByBBox(rgbFrameBitmap, p00, p11, 128, 128);
                }
                ArrayList<Classifier.Recognition> tret = null;
                if(faceAnalyzer != null) {
                    int inpW = faceAnalyzer.inpOut.dataInp.shape.get(0);
                    int inpH = faceAnalyzer.inpOut.dataInp.shape.get(1);
                    Bitmap bface = _cropBitmapByBBox(croppedBitmap, p00, p11, inpW, inpH);
                     tret = new ArrayList<>(faceAnalyzer.recognizeImage(bface));
                } else {
                    tret = new ArrayList<>();
                    tret.add(new Classifier.Recognition("", "*", 1.f, null));
                }

                Classifier.Recognition recFace = tret.get(0);
                recFace.setLocation(rect);
                results.add(recFace);
            }
//        }

        runInBackground(
            new Runnable() {
                @Override
                public void run() {
                    final long startTime = SystemClock.uptimeMillis();
                    lastProcessingTimeMs = SystemClock.uptimeMillis() - startTime;
                    //
                    cropCopyBitmap = Bitmap.createBitmap(croppedBitmap);
                    final Canvas canvas = new Canvas(cropCopyBitmap);
                    final Paint paint = new Paint();
                    paint.setColor(Color.RED);
                    paint.setStyle(Style.STROKE);
                    paint.setStrokeWidth(2.0f);

                    final List<Classifier.Recognition> mappedRecognitions = new LinkedList<Classifier.Recognition>();

                    for (final Classifier.Recognition result : results) {
                        final RectF location = result.getLocation();
                        if (location != null && result.getConfidence() >= MINIMUM_CONFIDENCE) {
                            canvas.drawRect(location, paint);
                            cropToFrameTransform.mapRect(location);
                            result.setLocation(location);
                            mappedRecognitions.add(result);
                        }
                    }

                    tracker.trackResults(mappedRecognitions, luminance, currTimestamp);
                    trackingOverlay.postInvalidate();

                    requestRender();
                    computing = false;
                }
            });

        Trace.endSection();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.camera_connection_fragment_tracking;
    }

    @Override
    protected Size getDesiredPreviewFrameSize() {
        return DESIRED_PREVIEW_SIZE;
    }

    @Override
    public void onSetDebug(final boolean debug) {
        if(faceAnalyzer != null) {
            faceAnalyzer.enableStatLogging(debug);
        }
    }

    private void _loadSelectedModel() {
        int modelPos = spinModels.getSelectedItemPosition();
        String modelName = modelList.get(modelPos);
        LOGGER.d(":: loading model: [" + modelName + "]");
        faceAnalyzer = (TensorFlowFaceAnalyser) TensorFlowFaceAnalyser.create(getAssets(), MODEL_DIR + "/" + modelName);
    }

    private void _updateModels() {
        Log.d(TAG, ":: btnPush()");
        modelList = new ArrayList<>();
        String[] list = null;
        try {
            list = getAssets().list(MODEL_DIR);
            for (String s : list) {
                if (s.endsWith(".pb")) {
                    modelList.add(s);
                }
            }
            spinModelsAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1);
            spinModelsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinModels.setAdapter(spinModelsAdapter);
            for (String s : modelList) {
                spinModelsAdapter.add(s);
            }
            Log.d(TAG, "FUCK");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_loadModel:
                Log.d(TAG, "::loadModel()");
                _loadSelectedModel();
                break;
            case R.id.btn_goToTheNextCamera:
                Log.d(TAG, "::goToTheNextCamera()");
                goToTheNextCamera();
                break;
        }
    }
}
