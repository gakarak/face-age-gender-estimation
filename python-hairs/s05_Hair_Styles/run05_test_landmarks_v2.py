#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ar'

import os
import numpy as np
import skimage.io as skio
import skimage.transform as sktf
import matplotlib.pyplot as plt

import cv2
import dlib.dlib

from lxml import html
from bs4 import BeautifulSoup

#########################################
def getFaceRectangels(detResults):
    ret = []
    for rr in detResults:
        x0 = rr.left()
        y0 = rr.top()
        x1 = rr.right()
        y1 = rr.bottom()
        tmp = []
        tmp.append((x0, y0))
        tmp.append((x1, y0))
        tmp.append((x1, y1))
        tmp.append((x0, y1))
        tmp.append((x0, y0))
        ret.append(np.array(tmp))
    return ret

def getListOfPts(pparts):
    numPts = pparts.num_parts
    ret = []
    for ii in range(numPts):
        tp = pparts.part(ii)
        ret.append((tp.x, tp.y))
    return np.array(ret)

#########################################
class FaceDetector:
    pathDlibFaceDetector = ''
    pathDlibFaceLandmarks = '/Users/alexanderkalinovsky/i/dlib.net/shape_predictor_68_face_landmarks.dat'
    def __init__(self):
        self.detectorFace = dlib.get_frontal_face_detector()
        self.predictorLand = dlib.shape_predictor(self.pathDlibFaceLandmarks)

    def detectLandmarks(self, pimg):
        if isinstance(pimg, str) or isinstance(pimg, unicode):
            pimg = skio.imread(pimg)
        facesDet = self.detectorFace(pimg, 1)
        lstFaceRects = getFaceRectangels(facesDet)
        if len(lstFaceRects) < 1:
            strError = 'Cant find face on image'
            # raise Exception('Cant find face on image [%s] with face :(' % fimg)
            print ('\tERROR: %s, skip...' % strError)
            return (None,None)
        frect = lstFaceRects[0]
        # rc00 = frect[0, ::-1]
        # rc11 = frect[2, ::-1]
        # faceCrop = pimg[rc00[0]:rc11[0], rc00[1]:rc11[1], :]
        # faceCropRect = dlib.rectangle(4, 4, rc11[1] - rc00[1] - 4, rc11[0] - rc00[0] - 4)
        faceRect = facesDet[0]
        # shape = self.predictorLand(pimg, faceCropRect)
        shape = self.predictorLand(pimg, faceRect)
        shapePts = getListOfPts(shape)
        return (frect, shapePts)

#########################################
def warpAffinePts(pmat, ppts):
    ptsOnes = np.concatenate([ppts, np.ones((ppts.shape[0], 1))], axis=1)
    ret = np.dot(pmat, ptsOnes.transpose()).transpose()
    return ret

def getMatShift(dx, dy):
    matShift = np.zeros((2, 3))
    matShift[0, 0] = 1.0
    matShift[0, 1] = 0.0
    matShift[1, 0] = 0.0
    matShift[1, 1] = 1.0
    matShift[0, 2] = dx
    matShift[1, 2] = dy
    return matShift

def affineShiftImage(pimg, xyShift, outShape=None):
    if outShape is None:
        outShape = pimg.shape[:2]
    dx1 = xyShift[0]
    dy1 = xyShift[1]
    matShift1 = getMatShift(dx1, dy1)
    imgShift1 = cv2.warpAffine(pimg, matShift1, (int(outShape[1]), int(outShape[0])), None, cv2.INTER_CUBIC)
    return imgShift1

#########################################
def matchCorr(pimg1, pimg2):
    pmsk = pimg2[:, :, 3].copy()
    pimg1 = pimg1[:, :, :3]
    pimg2 = pimg2[:, :, :3]
    if pimg1.ndim>2:
        pimg1 = cv2.cvtColor(pimg1, cv2.COLOR_BGR2GRAY)
    if pimg2.ndim>2:
        pimg2 = cv2.cvtColor(pimg2, cv2.COLOR_BGR2GRAY)
    pimg1 = pimg1.astype(np.float) / 255.
    pimg2 = pimg2.astype(np.float) / 255.
    f1 = np.fft.fft2(pimg1)
    f2 = np.fft.fft2(pimg2)
    ret = np.abs(np.fft.ifft2(f1 * f2))
    ret *= pmsk
    return ret

#########################################
def readFileTxt(ppath):
    with open(ppath, 'r') as f:
        return f.read()

def parseXmlWithPoints(dataXml):
    soupSWF = BeautifulSoup(dataXml, "lxml")
    list_pts = soupSWF.find_all('point')
    arrAnchors = []
    arrControl = []
    for ipts, pts in enumerate(list_pts):
        anchorx  = float(pts.attrs['anchorx'])
        anchory  = float(pts.attrs['anchory'])
        controlx = float(pts.attrs['controlx'])
        controly = float(pts.attrs['controly'])
        arrAnchors.append([anchorx, anchory])
        arrControl.append([controlx, controly])
    return (np.array(arrControl), np.array(arrAnchors))

lstIdxOval = list(range(17))
lstIdxNose = list(range(27,36))
lstIdxMatch = lstIdxOval + lstIdxNose


################################
if __name__ == '__main__':
    # pathImg = '/Users/alexanderkalinovsky/tmp/111/0087_Wavy.png'
    # pathThmb = '/Users/alexanderkalinovsky/tmp/111/0087_Wavy_Thumb.jpg'
    # pathOffs = '/Users/alexanderkalinovsky/tmp/111/0087_Wavy_Offset.png'

    pathHairBasic = '/Users/alexanderkalinovsky/tmp/111/0087_Wavy.png'
    pathOffs = '/Users/alexanderkalinovsky/tmp/111/0087_Wavy_Offset.png'
    pathFaceGuide = '/Users/alexanderkalinovsky/tmp/111/face-guide.jpg'

    pathFace = '/Users/alexanderkalinovsky/tmp/111/front_view_images_10000_original_Bailee-Madison_00.jpg'
    pathHair = '/Users/alexanderkalinovsky/tmp/111/front_view_images_10000_original_Bailee-Madison_00.jpg_thumb.png'
    # pathOffs = '/Users/alexanderkalinovsky/tmp/111/front_view_images_10000_original_Bailee-Madison_00.jpg_offset.png'
    #
    pathOval = '/Users/alexanderkalinovsky/tmp/111/pts_oval.xml'
    pathShoulders = '/Users/alexanderkalinovsky/tmp/111/pts_shoulders.xml'
    pathOval = '/Users/alexanderkalinovsky/tmp/111/pts_oval.xml'
    pathLips = '/Users/alexanderkalinovsky/tmp/111/pts_lips.xml'
    # (1) load images
    imgHairBasic = skio.imread(pathHairBasic)
    imgFaceGuide = skio.imread(pathFaceGuide)
    imgFace = skio.imread(pathFace)
    imgOffset = skio.imread(pathOffs)
    imgHair = skio.imread(pathHair)
    # (2) load pts-info
    xyAnchOval, xyCtrlOval = parseXmlWithPoints(readFileTxt(pathOval))
    xyAnchShoulders, xyCtrlShoulders = parseXmlWithPoints(readFileTxt(pathShoulders))
    xyAnchLips, xyCtrlLips = parseXmlWithPoints(readFileTxt(pathLips))
    # (3) datact face and landmarks
    faceDetector = FaceDetector()
    faceBBox, faceLandmarks = faceDetector.detectLandmarks(imgFace)
    faceGuideBBox, faceGuideLandmarks = faceDetector.detectLandmarks(imgFaceGuide)

    # (4) prepare basic image-offset
    shapeOffset = np.array(imgOffset.shape, dtype=np.float)
    shapeImg = np.array(imgHairBasic.shape, dtype=np.float)

    # (5) set basic shifts
    # xyShift = [105, 75]
    # xyBBox = np.array([[105, 75], [398, 428]])
    xyShift = [106, 76]
    xyBBox = np.array([[106, 76], [399, 429]])

    shpXYMin = np.min(xyAnchOval, axis=0)
    shpXYMax = np.max(xyAnchOval, axis=0)
    xyBBoxShape = np.vstack((shpXYMin, shpXYMax))

    xyBBoxMean = np.mean(xyBBox, axis=0)
    xyBBoxShapeMean = np.mean(xyBBoxShape, axis=0)

    # (6) prepare guoded face
    xyShiftGuided = (- xyBBoxShapeMean[0] + xyBBoxMean[0], - xyBBoxShapeMean[1] + xyBBoxMean[1])
    imgFaceGuide2 = affineShiftImage(imgFaceGuide, xyShiftGuided, outShape=imgHairBasic.shape[:2])

    faceGuideLandmarksShift = warpAffinePts(getMatShift(xyShiftGuided[0], xyShiftGuided[1]), faceGuideLandmarks)

    imgOffsetR = sktf.resize(imgOffset, imgHairBasic.shape[:3], order=0)


    #
    matTrfLandmarks = cv2.estimateRigidTransform(faceLandmarks[lstIdxMatch,:].astype(np.float32),
                                                 faceGuideLandmarksShift[lstIdxMatch,:].astype(np.float32),
                                                 fullAffine=False)
    # imgFaceTrf = cv2.warpAffine(imgFace, matTrfLandmarks[0], imgFaceGuide2.shape[:2])
    imgFaceTrf = cv2.warpAffine(imgFace, matTrfLandmarks, imgFaceGuide2.shape[:2][::-1])

    #
    CC = matchCorr(imgFaceTrf, imgHair)

    plt.subplot(1,2,1)
    plt.imshow(imgFace)
    plt.plot(faceLandmarks[:,0], faceLandmarks[:,1], 'o')
    plt.subplot(1, 2, 2)
    plt.imshow(imgFaceTrf)
    plt.imshow(imgHair)
    plt.plot(faceGuideLandmarksShift[:,0], faceGuideLandmarksShift[:,1], 'o')
    plt.show()


    plt.subplot(2, 2, 1)
    plt.imshow(imgOffsetR[:,:,0])
    plt.imshow(imgHair)
    plt.plot(xyAnchOval[:, 0] - xyBBoxShapeMean[0] + xyBBoxMean[0],
             xyAnchOval[:, 1] - xyBBoxShapeMean[1] + xyBBoxMean[1], 'o')
    plt.plot(xyCtrlOval[:, 0] - xyBBoxShapeMean[0] + xyBBoxMean[0],
             xyCtrlOval[:, 1] - xyBBoxShapeMean[1] + xyBBoxMean[1], 'o')
    plt.plot(xyAnchShoulders[:, 0] - xyBBoxShapeMean[0] + xyBBoxMean[0],
             xyAnchShoulders[:, 1] - xyBBoxShapeMean[1] + xyBBoxMean[1], 'o')
    plt.plot(xyCtrlLips[:, 0] - xyBBoxShapeMean[0] + xyBBoxMean[0],
             xyCtrlLips[:, 1] - xyBBoxShapeMean[1] + xyBBoxMean[1], '.')
    plt.grid(True)

    plt.subplot(2, 2, 2)
    plt.imshow(imgFace)
    plt.plot(faceBBox[:,0], faceBBox[:, 1], 'o')
    plt.plot(faceLandmarks[lstIdxMatch, 0], faceLandmarks[lstIdxMatch, 1], '.')

    plt.subplot(2, 2, 3)
    plt.imshow(imgFaceGuide)
    plt.plot(xyAnchOval[:, 0], xyAnchOval[:, 1], 'o')
    plt.plot(xyCtrlOval[:, 0], xyCtrlOval[:, 1], 'o')
    plt.plot(faceGuideLandmarks[lstIdxMatch,0], faceGuideLandmarks[lstIdxMatch,1], '.')

    plt.subplot(2, 2, 4)
    plt.imshow(imgFaceGuide2)
    plt.plot(xyAnchOval[:, 0] - xyBBoxShapeMean[0] + xyBBoxMean[0],
             xyAnchOval[:, 1] - xyBBoxShapeMean[1] + xyBBoxMean[1], 'o')
    plt.plot(xyCtrlOval[:, 0] - xyBBoxShapeMean[0] + xyBBoxMean[0],
             xyCtrlOval[:, 1] - xyBBoxShapeMean[1] + xyBBoxMean[1], 'o')
    plt.plot(faceGuideLandmarksShift[lstIdxMatch,0], faceGuideLandmarksShift[lstIdxMatch,1], '.')

    # plt.plot(xyAnchOval[:, 0], xyAnchOval[:, 1], 'o')
    # plt.plot(xyCtrlOval[:, 0], xyCtrlOval[:, 1], '.')
    # plt.subplot(1,3,3)
    # plt.imshow(img)
    # plt.plot(xyAnchOval[:, 0] + xyShift[0], xyAnchOval[:, 1] + xyShift[1], '.')
    # plt.plot(xyCtrlOval[:, 0] + 0 * xyShift[0], xyCtrlOval[:, 1] + 0 * xyShift[1], '.')
    plt.show()
    print ('---')