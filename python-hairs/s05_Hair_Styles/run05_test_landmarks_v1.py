#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ar'

import os
import numpy as np
import skimage.io as skio
import skimage.transform as sktf
import matplotlib.pyplot as plt

from lxml import html
from bs4 import BeautifulSoup

def readFileTxt(ppath):
    with open(ppath, 'r') as f:
        return f.read()

def parseXmlWithPoints(dataXml):
    soupSWF = BeautifulSoup(dataXml, "lxml")
    list_pts = soupSWF.find_all('point')
    arrAnchors = []
    arrControl = []
    for ipts, pts in enumerate(list_pts):
        anchorx  = float(pts.attrs['anchorx'])
        anchory  = float(pts.attrs['anchory'])
        controlx = float(pts.attrs['controlx'])
        controly = float(pts.attrs['controly'])
        arrAnchors.append([anchorx, anchory])
        arrControl.append([controlx, controly])
    return (np.array(arrControl), np.array(arrAnchors))


################################
if __name__ == '__main__':
    pathImg = '/Users/alexanderkalinovsky/tmp/111/0087_Wavy.png'
    pathThmb = '/Users/alexanderkalinovsky/tmp/111/0087_Wavy_Thumb.jpg'
    pathOval = '/Users/alexanderkalinovsky/tmp/111/oval.xml'
    pathOffs = '/Users/alexanderkalinovsky/tmp/111/0087_Wavy_Offset.png'
    #
    img = skio.imread(pathImg)
    imgOffset = skio.imread(pathOffs)
    imgThmb = skio.imread(pathThmb)
    xmlOval = readFileTxt(pathOval)
    xyAnch, xyCtrl = parseXmlWithPoints(xmlOval)
    shapeOffset = np.array(imgOffset.shape, dtype=np.float)
    shapeImg = np.array(img.shape, dtype=np.float)

    xyShift = [105, 75]
    xyBBox = np.array([[105, 75], [398, 428]])

    shpXYMin = np.min(xyAnch, axis=0)
    shpXYMax = np.max(xyAnch, axis=0)
    xyBBoxShape = np.vstack((shpXYMin, shpXYMax))

    xyBBoxMean = np.mean(xyBBox, axis=0)
    xyBBoxShapeMean = np.mean(xyBBoxShape, axis=0)




    imgOffsetR = sktf.resize(imgOffset, img.shape[:3], order=0)

    plt.subplot(1, 3, 1)
    plt.imshow(imgOffsetR[:,:,0])
    plt.imshow(img)
    plt.plot(xyAnch[:, 0] - xyBBoxShapeMean[0] + xyBBoxMean[0], xyAnch[:, 1] - xyBBoxShapeMean[1] + xyBBoxMean[1], 'o')

    plt.subplot(1, 3, 2)
    plt.imshow(imgOffset)
    plt.plot(xyAnch[:, 0], xyAnch[:, 1], 'o')
    plt.plot(xyCtrl[:, 0], xyCtrl[:, 1], '.')
    plt.subplot(1,3,3)
    plt.imshow(img)
    plt.plot(xyAnch[:, 0] + xyShift[0], xyAnch[:, 1] + xyShift[1], '.')
    plt.plot(xyCtrl[:, 0] + 0*xyShift[0], xyCtrl[:, 1] + 0*xyShift[1], '.')
    plt.show()
    print ('---')