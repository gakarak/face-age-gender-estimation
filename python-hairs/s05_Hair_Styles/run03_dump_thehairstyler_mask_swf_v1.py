#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ar'

import numpy as np
import os
import time
import shutil
import requests
import json
import skimage.io as skio

from lxml import html
from bs4 import BeautifulSoup


try:
    from cStringIO import StringIO
except:
    from StringIO import StringIO

def downloadImage(urlRequest, pauthTocken=None):
    tret = requests.get(urlRequest, stream=False)
    if tret.status_code == 200:
        buff = StringIO()
        for chunk in tret.iter_content(2048):
            buff.write(chunk)
        return buff
    else:
        strErr = 'Error: %s' % tret._content
        print('*** ERROR: %s : %s' % (strErr, urlRequest))
        return None

def httpRequest(purl):
    tret = requests.get(purl, stream=False)
    if tret.status_code == 200:
        return tret._content
    else:
        strErr = 'Error: %s' % tret._content
        print('*** ERROR: %s : %s' % (strErr, purl))
        return None

def dumpImageFromURL(purl, baseFileName):
    try:
        tmpPref = os.path.basename(purl)
        tmpOutFileName = '{0}_{1}'.format(baseFileName, tmpPref)
        if os.path.isfile(tmpOutFileName):
            print ('\t@file exist@, skip... {0}'.format(tmpOutFileName))
            return None
        imageBuff = downloadImage(purl)
        if imageBuff is not None:
            with open(tmpOutFileName, 'wb') as f:
                f.write(imageBuff.getvalue())
        # appendSting2File(fidx, strLog)
    except Exception as err:
        print ('\t***ERROR*** cant process: {0} for url [{0}], skip...'.format(err, urlImg))

def appendSting2File(fpath, pstr):
    with open(fpath, 'a') as f:
        f.write('{0}\n'.format(pstr))

if __name__ == '__main__':
    # list_genders = ['mens', 'womens']
    # list_types = ['salon', 'celebrity']
    list_genders = ['womens', 'mens']
    list_types = ['salon', 'celebrity']
    # urlTemplate = 'http://www.thehairstyler.com/hairstyles/search?category_name%5B%5D=casual&category_name%5B%5D=formal&category_name%5B%5D=alternative&category_source%5B%5D=celebrity&commit=Find+hairstyles&gender%5B%5D=mens&page=2&q='
    urlTemplate = 'http://www.thehairstyler.com/hairstyles/search?category_name%5B%5D=casual&category_name%5B%5D=formal&category_name%5B%5D=alternative&gender%5B%5D={0}&length%5B%5D=short&length%5B%5D=medium&length%5B%5D=long&elasticity%5B%5D=straight&elasticity%5B%5D=wavy&elasticity%5B%5D=curly&style%5B%5D=up&style%5B%5D=down&style%5B%5D=halfup&texture%5B%5D=fine&texture%5B%5D=medium&texture%5B%5D=coarse&category_source%5B%5D={1}&page={2}&q=&commit=Find+hairstyles'
    urlSwfMetaTmpl = 'http://www.thehairstyler.com/api/hairstyle?person_id=-1&id={0}&'
    #
    baseDir = '/Users/alexanderkalinovsky/data/@Virtual_Hairs/Style_My_Hair/02_tmp'
    pathTmp = '/Users/alexanderkalinovsky/data/@Virtual_Hairs/Style_My_Hair/02_tmp/q.html'
    maxNumPages = 300
    #
    wdir = os.path.dirname(pathTmp)
    for igender, gender in enumerate(list_genders):
        for itype, ptype in enumerate(list_types):
            wdir = '{0}/{1}_{2}'.format(baseDir, gender, ptype)
            fidx = '{0}/idx.txt'.format(wdir)
            if not os.path.isdir(wdir):
                os.makedirs(wdir)
            for idxPage in range(maxNumPages):
                print ('[{0}/{1}] [{2}/{3}]'.format(gender, ptype, idxPage, maxNumPages))
                urlPage = urlTemplate.format(gender, ptype, idxPage+1)
                htmlDataResp = requests.get(urlPage)
                if htmlDataResp.status_code == 200:
                    htmlData = htmlDataResp._content
                    # tree = html.fromstring(htmlData)
                    soup = BeautifulSoup(htmlData, "lxml")
                    hair_list = soup.find_all('div', {'class': 'hairstyle_image'})
                    if len(hair_list)<1:
                        print ('\t!!!WARNING!!! cant find more images, skip... [{0}]'.format(urlPage))
                        break
                    # hair_list = soup.find_all('div', {'class': 'hairstyle_large_image_wrapper hairstyle_slider'})
                    cnt = 0
                    for itagDiv, tagDiv in enumerate(hair_list):
                        styleName = tagDiv.find('a').text.strip()
                        tagA_list = tagDiv.find_all('a', {'class': 'try_on_link'})
                        idxImg = 0
                        for itagA, tagA in enumerate(tagA_list):
                            relID = tagA.attrs['rel'][0]
                            tagImg = tagA.find('img', {'class': 'hairstyle_image_item'})
                            if tagImg is None:
                                continue
                            urlImg = tagImg.attrs['src']
                            txtImg = tagImg.attrs['alt'].strip()
                            #
                            try:
                                tmpFN = urlImg.split('hairstyle_views/')[1].replace('/','_')
                            except Exception as err:
                                print ('## Unknown Error: [{0}]'.format(err))
                                continue
                            # urlFileName = os.path.basename(urlImg)
                            urlFileName, urlFileExt = os.path.splitext(tmpFN)
                            # foutFileName = '{3}/{0:04d}_{1:02d}_{2}'.format(cnt, idxImg, urlFileName, wdir)
                            foutFileName = '{0}/{1}_{2:02d}{3}'.format(wdir, urlFileName, idxImg, urlFileExt)
                            #
                            swfInfoURL = urlSwfMetaTmpl.format(relID)
                            swfInfoResponse = httpRequest(swfInfoURL)
                            if swfInfoResponse is None:
                                continue
                            soupSWF = BeautifulSoup(swfInfoResponse, "lxml")
                            swfData = soupSWF.find('hairstyle')
                            #
                            urlHairBase = swfData['hair']
                            urlHairHighlight = swfData['highlight']
                            urlOffset = swfData['offset']
                            urlOriginal = swfData['original']
                            urlShadows = swfData['shadow']
                            urlThumb = swfData['thumb']
                            #
                            dumpImageFromURL(urlHairBase, foutFileName)
                            dumpImageFromURL(urlHairHighlight, foutFileName)
                            dumpImageFromURL(urlOffset, foutFileName)
                            dumpImageFromURL(urlOriginal, foutFileName)
                            dumpImageFromURL(urlShadows, foutFileName)
                            dumpImageFromURL(urlThumb, foutFileName)
                            foutMetaData = '{0}-meta.xml'.format(foutFileName)
                            with open(foutMetaData, 'w') as f:
                                f.write('{0}\n'.format(swfData))
                            #
                            # if not os.path.isfile(foutFileName):
                            #     strLog = '{0},{1},{2},{3}'.format(os.path.basename(foutFileName), gender, ptype, styleName)
                            #     #
                            #     try:
                            #         imageBuff = downloadImage(urlImg)
                            #         # if imageBuff is not None:
                            #         #     with open(foutFileName, 'wb') as f:
                            #         #         f.write(imageBuff.getvalue())
                            #         # appendSting2File(fidx, strLog)
                            #     except Exception as err:
                            #         print ('\t***ERROR*** cant process: {0} for url [{0}], skip...'.format(err, urlImg))
                            # else:
                            #     print ('*warning* file exist, skip ... [{0}]'.format(foutFileName))
                            idxImg += 1
                            print ('\t::{0}'.format(urlImg))
                            if idxImg>0:
                                continue
                        cnt += 1