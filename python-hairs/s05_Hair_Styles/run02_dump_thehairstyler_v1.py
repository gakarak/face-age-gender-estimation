#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ar'

import numpy as np
import os
import time
import shutil
import requests
import json
import skimage.io as skio

from lxml import html
from bs4 import BeautifulSoup


try:
    from cStringIO import StringIO
except:
    from StringIO import StringIO

def downloadImage(urlRequest, pauthTocken=None):
    tret = requests.get(urlRequest, stream=False)
    if tret.status_code == 200:
        buff = StringIO()
        for chunk in tret.iter_content(2048):
            buff.write(chunk)
        return buff
    else:
        strErr = 'Error: %s' % tret._content
        print('*** ERROR: %s : %s' % (strErr, urlRequest))
        return None


if __name__ == '__main__':
    list_genders = ['womens', 'mens']
    list_types = ['salon', 'celebrity']
    # urlTemplate = 'http://www.thehairstyler.com/hairstyles/search?category_name%5B%5D=casual&category_name%5B%5D=formal&category_name%5B%5D=alternative&category_source%5B%5D=celebrity&commit=Find+hairstyles&gender%5B%5D=mens&page=2&q='
    urlTemplate = 'http://www.thehairstyler.com/hairstyles/search?category_name%5B%5D=casual&category_name%5B%5D=formal&category_name%5B%5D=alternative&gender%5B%5D={0}&length%5B%5D=short&length%5B%5D=medium&length%5B%5D=long&elasticity%5B%5D=straight&elasticity%5B%5D=wavy&elasticity%5B%5D=curly&style%5B%5D=up&style%5B%5D=down&style%5B%5D=halfup&texture%5B%5D=fine&texture%5B%5D=medium&texture%5B%5D=coarse&category_source%5B%5D={1}&page={2}&q=&commit=Find+hairstyles'
    #
    baseDir = '/Users/alexanderkalinovsky/data/@Virtual_Hairs/Style_My_Hair/02_tmp'
    pathTmp = '/Users/alexanderkalinovsky/data/@Virtual_Hairs/Style_My_Hair/02_tmp/q.html'
    maxNumPages = 300

    #
    wdir = os.path.dirname(pathTmp)
    with open(pathTmp, 'r') as f:
        htmlData = f.read()
        # tree = html.fromstring(htmlData)
        soup = BeautifulSoup(htmlData, "lxml")
        hair_list = soup.find_all('div', {'class': 'hairstyle_image'})
        # hair_list = soup.find_all('div', {'class': 'hairstyle_large_image_wrapper hairstyle_slider'})
        cnt = 0
        for itagDiv, tagDiv in enumerate(hair_list):
            styleName = tagDiv.find('a').text.strip()
            imag_list = tagDiv.find_all('img', {'class' : 'hairstyle_image_item'})
            idxImg = 0
            for itagImg, tagImg in enumerate(imag_list):
                urlImg = tagImg.attrs['src']
                txtImg = tagImg.attrs['alt'].strip()
                #
                urlFileName = os.path.basename(urlImg)
                foutFileName = '{3}/{0:04d}_{1:02d}_{2}'.format(cnt, idxImg, urlFileName, wdir)
                #
                imageBuff = downloadImage(urlImg)
                if imageBuff is not None:
                    with open(foutFileName, 'wb') as f:
                        f.write(imageBuff.getvalue())
                idxImg += 1
                print ('---')
            cnt += 1