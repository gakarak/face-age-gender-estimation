#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ar'


import numpy as np
import os
import time
import shutil
import requests
import json
import skimage.io as skio

try:
    from cStringIO import StringIO
except:
    from StringIO import StringIO

def downloadImage(urlRequest, pauthTocken=None):
    tret = requests.get(urlRequest, stream=False)
    if tret.status_code == 200:
        buff = StringIO()
        for chunk in tret.iter_content(2048):
            buff.write(chunk)
        return buff
    else:
        strErr = 'Error: %s' % tret._content
        print('*** ERROR: %s : %s' % (strErr, urlRequest))
        return None


if __name__ == '__main__':
    pathHairs = '/Users/alexanderkalinovsky/data/@Virtual_Hairs/Style_My_Hair/all_hairs_with_faceboxes.txt'
    pathStyle = '/Users/alexanderkalinovsky/data/@Virtual_Hairs/Style_My_Hair/all_hair_styles.txt'
    pathEfect = '/Users/alexanderkalinovsky/data/@Virtual_Hairs/Style_My_Hair/all_hair_effects.txt'
    urlRequestTmp = 'http://makeovr.com/lp_hair/proxy.php?psx=24&action=hairapi&apiserver=184.106.134.179&appmode=hairstyle&picid=Hairstyle_Jan2016_2.png&hairid=Hairstyle_Jan2016_2.png&facecoords=215,86,378,298&coloramount=50&coloreffect=uniform&highlightid=Hairstyle_Jan2016_2_highlights.png&colorpatch=hair%2Fcolor%2Fimg%2F4_20.png&cachefix=0.5948729979284644'
    urlRequestPref = 'http://makeovr.com/lp_hair/proxy.php?psx=24&action=hairapi&apiserver=184.106.134.179&appmode=hairstyle&picid={0}.png&hairid={0}.png&facecoords={1}&coloramount={2}&coloreffect={3}&highlightid={0}_highlights.png&colorpatch=hair%2Fcolor%2Fimg%2F{4}.png&cachefix={5:0.16f}'

    wdir = os.path.dirname(pathHairs)
    dirOut = '%s_out' % wdir
    if not os.path.isdir(dirOut):
        os.makedirs(dirOut)
    #
    foutImageTmpl = '{0}--{1}-{2}-{3}.png'

    urlReuestImage = 'http://184.106.134.179/lp_hair/img/{0}.png'
    with open(pathHairs, 'r') as f:
        lstHairs = f.read().splitlines()
        lstHairs = [xx.split('|') for xx in lstHairs ]
    with open(pathStyle, 'r') as f:
        lstStyles = f.read().splitlines()
    with open(pathEfect, 'r') as f:
        lstEffects = f.read().splitlines()

    arrColors = np.linspace(5,100, 8).tolist()

    for ihair, hair in enumerate(lstHairs):
        for istyle, style in enumerate(lstStyles):
            # tmpRnd = 0.5948729979284644
            for ieff, eff in enumerate(lstEffects):
                tmpRnd = np.random.rand()
                for icolor, color in enumerate(arrColors):
                    hairName = os.path.splitext(hair[0])[0]
                    faceBox  = hair[1]
                    colorPatch = os.path.splitext(os.path.basename(style))[0]
                    turlReuestKey = urlRequestPref.format(hairName, faceBox, color, eff, colorPatch, tmpRnd)
                    foutImage = '{0}/{1}'.format(dirOut, foutImageTmpl.format(hairName, colorPatch, eff, int(color)))
                    if os.path.isfile(foutImage):
                        print ('*** warning *** file [{0}] exist, skip...'.format(foutImage))
                        continue
                    tretKey = requests.get(turlReuestKey)
                    if tretKey.status_code == 200:
                        tkeyAPI = tretKey._content
                        turlReuestImg = urlReuestImage.format(tkeyAPI)
                        tretImageBuff = downloadImage(turlReuestImg)
                        if tretImageBuff is not None:
                            with open(foutImage, 'wb') as f:
                                f.write(tretImageBuff.getvalue())
                        print (':: [{0}]'.format(turlReuestKey))
