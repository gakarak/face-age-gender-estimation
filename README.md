# Android DEMO application Gender/Age estimation #

This README would normally document whatever steps are necessary to get your application up and running.

```
mkdir -p ~/Workspace_TF
cd ~/Workspace_TF
```

# (1) Устанавливаем python-версию TF & keras (не паримся с GPU - ставим CPU-версию)
```
pip install tensorflow -U
pip install keras -U
pip install h5py -U
```

Скачиваем TF:
```
wget https://github.com/tensorflow/tensorflow/archive/v1.1.0.zip
```

```
unzip tensorflow-1.1.0.zip
```

```
cd tensorflow-1.1.0
./configure
```

```
bazel build --config=opt //tensorflow/tools/pip_package:build_pip_package
```


```
bazel build tensorflow/python/tools:freeze_graph
bazel build tensorflow/python/tools:optimize_for_inference
bazel build tensorflow/tools/quantization:quantize_graph
```


Клонируем репозиторий в каталог:

```
cd ~/Workspace_TF/tensorflow-1.1.0/tensorflow/examples
git clone https://bitbucket.org/gakarak/face-age-gender-estimation/ face-age-gender-estimation
ln -s face-age-gender-estimation/android-demo android-demo
```

Копируемуем пример моделей из каталога data-examples в assets/models:

```
mkdir -p android-demo/assets/models
cp face-age-gender-estimation/data-examples/models/* android-demo/assets/models
```

после чего откруваем проект android-demo в AndroidStudio и запускаем.

## Конвертация обученных моделей из формата Keras в Tensorflow

Можно воспользоваться python-скриптом для конвертации Keras в оптимизированное для mobile и квантизованное представление
на TF. Скрипт находится в каталоге "face-age-gender-estimation.git/python-model-processing"

```
cd face-age-gender-estimation.git/python-model-processing
./start12-convert-all-models-indir-q8.sh
```

SHELL-скрипт использует python-обёртку "run01_keras2tf_v2.py" для того, чтобы сконвертировать все keras модели из каталога
00_model_processing в TF-формат.
на выходе получатся файлы в подкаталогах с именами *-final-* и *-final-q8. В файлах *.json содержится описание входов/выходов сети.

Для работы SHELL/Python скрипта нужно указать корректный путь к скомпилированному Tnesorflow-SDK (переменная bdir в **start12-convert-all-models-indir-q8.sh**)
```
...
bdir="/Users/alexanderkalinovsky/Workspace/tensorflow-1.0.0-sdk/bazel-bin"
...
```