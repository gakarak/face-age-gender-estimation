#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ar'

import os
import keras
from keras.models import Sequential
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras.layers import Conv2D, SeparableConv2D,MaxPool2D, Input, Dense, Flatten,\
    Reshape,  UpSampling2D, Activation, BatchNormalization
from keras.optimizers import SGD, Adam
from keras.datasets import mnist
import numpy as np
from PIL import Image
import skimage.io as skio
import matplotlib.pyplot as plt
import argparse
import math
import time
from keras.utils import np_utils

##############################################
def_model_pref = 'model_MNIST_SimpleCNN'
def_model_tmpl = def_model_pref + '_{epoch:05d}_{val_loss:0.4f}.h5'

##############################################
def buildModel(pinp_shape = (28,28,1), pnum_cls = 10, pnum_hidden = 128, pkernel_size = 3, pnum_flt = 8, numConv=2):
    nflt = pnum_flt
    ncls = pnum_cls
    ksiz = (pkernel_size, pkernel_size)
    dataInp = Input(shape=pinp_shape)
    x = dataInp
    for ii in range(3):
        for cc in range(numConv):
            x = Conv2D(filters=nflt * (2 ** ii), kernel_size=ksiz, padding='same', activation='relu')(x)
            # x = Conv2D(filters=nflt * (2 ** ii), kernel_size=ksiz, padding='same', activation='relu')(x)
        x = MaxPool2D(pool_size=(2,2))(x)
    #
    x = Flatten()(x)
    if pnum_hidden is not None:
        x = Dense(units=pnum_hidden, activation='relu')(x)
    #
    x = Dense(units=ncls, activation='softmax')(x)
    #
    ret = Model(inputs=dataInp, outputs=x)
    return ret

def buildModelDW(pinp_shape = (28,28,1), pnum_cls = 10, pnum_hidden = 128, pkernel_size = 3, pnum_flt = 8, numConv=1):
    nflt = pnum_flt
    ncls = pnum_cls
    ksiz = (pkernel_size, pkernel_size)
    dataInp = Input(shape=pinp_shape)
    x = dataInp
    for ii in range(3):
        for cc in range(numConv):
            x = SeparableConv2D(filters=nflt * (2 ** ii), kernel_size=ksiz, padding='same', activation='relu')(x)
            # x = SeparableConv2D(filters=nflt * (2 ** ii), kernel_size=ksiz, padding='same', activation='relu')(x)
        x = MaxPool2D(pool_size=(2,2))(x)
    #
    x = Flatten()(x)
    if pnum_hidden is not None:
        x = Dense(units=pnum_hidden, activation='relu')(x)
    #
    x = Dense(units=ncls, activation='softmax')(x)
    #
    ret = Model(inputs=dataInp, outputs=x)
    return ret

def buildModelDW2(pinp_shape = (28,28,1), pnum_cls = 10, pnum_hidden = 128, pkernel_size = 3, pnum_flt = 8, numConv=1):
    nflt = pnum_flt
    ncls = pnum_cls
    ksiz = (pkernel_size, pkernel_size)
    dataInp = Input(shape=pinp_shape)
    x = dataInp
    for ii in range(3):
        for cc in range(numConv):
            x = SeparableConv2D(filters=nflt * (2 ** ii), kernel_size=ksiz, padding='same')(x)
            x = BatchNormalization()(x)
            x = Activation('relu')(x)
        x = MaxPool2D(pool_size=(2,2))(x)
    #
    x = Flatten()(x)
    if pnum_hidden is not None:
        x = Dense(units=pnum_hidden, activation='relu')(x)
    #
    x = Dense(units=ncls, activation='softmax')(x)
    #
    ret = Model(inputs=dataInp, outputs=x)
    return ret

##############################################
if __name__ == '__main__':
    (X_trn, Y_trn), (X_val, Y_val) = mnist.load_data()
    X_trn = (1. / 256.) * X_trn.reshape([X_trn.shape[0]] + list(X_trn.shape[1:]) + [1])
    X_val = (1. / 256.) * X_val.reshape([X_val.shape[0]] + list(X_val.shape[1:]) + [1])
    numCls = len(np.unique(Y_trn))
    inpShape = X_trn.shape[1:]
    Y_trn = np_utils.to_categorical(Y_trn, numCls)
    Y_val = np_utils.to_categorical(Y_val, numCls)
    #
    # model = buildModel()
    # model = buildModelDW()
    model = buildModelDW2()
    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['acc'])
    model.summary()
    #
    numEpochs = 5
    t0 = time.time()
    model.fit(X_trn, Y_trn,
              batch_size=128,
              epochs=numEpochs,
              validation_data=(X_val, Y_val))
    dt = time.time() - t0
    print ('---')
    print ('Train: #Epoch = {0}, dt/dtpe = {1:0.3f} (s) / {2:0.3f} (s/epoch)'.format(numEpochs, dt, dt/numEpochs))
    #
    numInfIter = 10
    t0 = time.time()
    lstRet = []
    for ii in range(numInfIter):
        lstRet.append(model.predict(X_trn))
    dt = time.time() - t0
    print ('Inference: #Iter = {0}, dt/dtpi = {1:0.3f} (s) / {2:0.3f} (s/epoch)'.format(numInfIter, dt, dt/numInfIter))


