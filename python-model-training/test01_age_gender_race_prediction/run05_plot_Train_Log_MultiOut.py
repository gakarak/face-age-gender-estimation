#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ar'

import os
import sys
import matplotlib.pyplot as plt
import pandas as pd
import time
import glob
import skimage.io as skio

if __name__ == '__main__':
    # pathModel = 'test_cnn_model_v1.h5'
    # pkey = 'gender'
    pkey = ['gender', 'age', 'race']
    pcol = ['g', 'r', 'b']
    pathModel = 'test_cnn_model_mout_{0}.h5'.format('-'.join(pkey))
    flog = '%s-log.csv' % pathModel
    if os.path.isfile(flog):
        # data = pd.read_csv(flog, header=None).as_matrix()
        cnt = 0
        while True:
            data = pd.read_csv(flog)
            plt.clf()
            tlegend = []
            plt.subplot(1, 2, 1)
            for ikey, key in enumerate(pkey):
                tkeyTrn = '{0}_loss'.format(key)
                tkeyVal = 'val_{0}_loss'.format(key)
                dataTrn = data[tkeyTrn].as_matrix()
                dataVal = data[tkeyVal].as_matrix()
                plt.plot(dataTrn, '-o')
                plt.plot(dataVal, '-')
                tlegend.append(tkeyTrn)
                tlegend.append(tkeyVal)
            plt.title('Loss')
            plt.grid(True)
            plt.legend(tlegend)
            #
            plt.subplot(1, 2, 2)
            for ikey, key in enumerate(pkey):
                tkeyTrn = '{0}_acc'.format(key)
                tkeyVal = 'val_{0}_acc'.format(key)
                dataTrn = data[tkeyTrn].as_matrix()
                dataVal = data[tkeyVal].as_matrix()
                plt.plot(dataTrn, '{0}-o'.format(pcol[ikey]))
                plt.plot(dataVal, '{0}-'.format(pcol[ikey]))
                tlegend.append(tkeyTrn)
                tlegend.append(tkeyVal)
            plt.title('Accuracy')
            plt.grid(True)
            plt.legend(tlegend)

            #
            # # dataIter = data['iter'].as_matrix()
            # dataLossTrn = data['loss'].as_matrix()
            # dataLossVal = data['val_loss'].as_matrix()
            # dataAccTrn = data['acc'].as_matrix()
            # dataAccVal = data['val_acc'].as_matrix()
            # plt.clf()
            # plt.subplot(1, 2, 1)
            # plt.plot(dataLossTrn)
            # plt.plot(dataLossVal)
            # plt.grid(True)
            # plt.legend(['loss-train', 'loss-validation'])
            # plt.subplot(1, 2, 2)
            # plt.plot(dataAccTrn)
            # plt.plot(dataAccVal)
            # plt.grid(True)
            # plt.legend(['acc-train', 'acc-validation'], loc='best')
            #
            plt.show(block=False)
            plt.pause(5)
            print (':: update: [{0}]'.format(cnt))
            cnt += 1
    else:
        print ('*** WARNING *** cant find log-file [{0}]'.format(flog))