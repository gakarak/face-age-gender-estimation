#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ar'

import os
import sys
import matplotlib.pyplot as plt
import pandas as pd
import time
import glob
import skimage.io as skio
import keras.models

from run01_train_cnn_predictor_v1 import readCSV

if __name__ == '__main__':
    pkey = 'gender'
    pathModel = 'test_cnn_model_{0}.h5'.format(pkey)
    flog = '%s-log.csv' % pathModel
    # fcsvVal = '/home/ar/data/@Face-Aging-Datasets/01_UTK_Faces/UTKFace-x128-idx.txt-val.txt'
    fcsvVal = '/Users/alexanderkalinovsky/data/@Face_Aging/01_UTKFace/UTKFace-x128-test-idx.txt'
    pkey = 'gender'
    x_val, y_val, _ = readCSV(fcsvVal, key=pkey)

    x_val1 = x_val[:128]
    y_val1 = y_val[:128]
    model = keras.models.load_model(pathModel)
    y_pred1 = model.predict(x_val1)

    plt.plot(y_val1[:,0])
    plt.plot(y_pred1[:,0])
    plt.legend(['val', 'pred'])
    plt.grid(True)
    plt.show()

    for ii in range(128):
        plt.subplot(1,2,1)
        plt.imshow(0.5 * (x_val1[ii] + 1.0))
        plt.title('Male Pred/Real: {0:0.3f}/{1:0.3f}'.format(y_pred1[ii][0], y_val1[ii][0]))
        plt.show()

    print ('-')