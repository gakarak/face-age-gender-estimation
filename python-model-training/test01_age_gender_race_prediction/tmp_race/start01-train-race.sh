#!/bin/bash

source "${HOME}/pyenv/env-keras2/bin/activate"

export CUDA_VISIBLE_DEVICES="0"

python run01_train_cnn_predictor_v1_race.py
