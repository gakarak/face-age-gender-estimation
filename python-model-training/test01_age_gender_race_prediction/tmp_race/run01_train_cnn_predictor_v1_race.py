#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ar'

import os
import time
import shutil
import matplotlib.pyplot as plt
import skimage.io as skio
import skimage.color as skcol
import numpy as np
import keras
from keras.layers import Conv2D, UpSampling2D, \
    Flatten, Activation, Reshape, MaxPooling2D, Input, Dense
from keras.models import Model
import keras.losses
import keras.callbacks as kall
import pandas as pd
import keras.preprocessing.image

from keras.utils.vis_utils import plot_model as kplot
from keras.utils import np_utils
from keras.utils.vis_utils import plot_model

def readCSV(pcsv, key='gender', numCls = None):
    wdir = os.path.dirname(pcsv)
    data = pd.read_csv(pcsv)
    tpath = data['path'].as_matrix()
    retPath = np.array(['{0}/{1}'.format(wdir, xx) for xx in tpath])
    retLbl = data[key].as_matrix()
    if numCls is None:
        numCls = len(np.unique(retLbl))
    retLbl = np_utils.to_categorical(retLbl, numCls)
    #
    retImg = []
    print (':: Loading images from [{0}]'.format(pcsv))
    numImg = len(retPath)
    for ii, pp in enumerate(retPath):
        timg = skio.imread(pp).astype(np.float32)/127.5 - 1.
        if timg.ndim<3:
            timg = skcol.gray2rgb(timg)
        retImg.append(timg)
        if (ii%100)==0:
            print ('\t[{0}/{1}] ...'.format(ii, numImg))
    #
    retImg = np.array(retImg)
    return (retImg, retLbl, retPath)

def buildModel(pinp_shape=(128,128,3), kernelSize = 3, numFlt = 8, numCls = 2, numHidden=256):
    dataInput = Input(shape=pinp_shape)
    # Conv #1
    x = Conv2D(filters=numFlt * (2 ** 0), kernel_size=(kernelSize, kernelSize),
                   padding='same', activation='relu')(dataInput)
    x = Conv2D(filters=numFlt * (2 ** 0), kernel_size=(kernelSize, kernelSize),
               padding='same', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    # Conv #2
    x = Conv2D(filters=numFlt * (2 ** 1), kernel_size=(kernelSize, kernelSize),
                   padding='same', activation='relu')(x)
    x = Conv2D(filters=numFlt * (2 ** 1), kernel_size=(kernelSize, kernelSize),
               padding='same', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    # Conv #3
    x = Conv2D(filters=numFlt * (2 ** 2), kernel_size=(kernelSize, kernelSize),
                   padding='same', activation='relu')(x)
    x = Conv2D(filters=numFlt * (2 ** 2), kernel_size=(kernelSize, kernelSize),
               padding='same', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    # Conv #4
    x = Conv2D(filters=numFlt * (2 ** 3), kernel_size=(kernelSize, kernelSize),
               padding='same', activation='relu')(x)
    x = Conv2D(filters=numFlt * (2 ** 3), kernel_size=(kernelSize, kernelSize),
               padding='same', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    # Conv #5
    x = Conv2D(filters=numFlt * (2 ** 4), kernel_size=(kernelSize, kernelSize),
               padding='same', activation='relu')(x)
    x = Conv2D(filters=numFlt * (2 ** 4), kernel_size=(kernelSize, kernelSize),
               padding='same', activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    #
    x = Flatten()(x)
    if numHidden is not None:
        x = Dense(units=numHidden, activation='relu')(x)
    x = Dense(units=numCls, activation='softmax')(x)
    #
    model = Model(inputs=dataInput, outputs=x)
    return model

def train_generator(dataX, dataY, batchSize=256, prndShift = 0.15, prndStd = 0.1, isRandomize = True):
    data_generator = keras.preprocessing.image.ImageDataGenerator(
        rotation_range=10,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True
    )
    while True:
        outX, outY = next(data_generator.flow(dataX, dataY, batch_size=batchSize))
        if isRandomize:
            trnd_mean = (np.random.random(outX.shape) - 0.5) * prndShift
            trnd_std  = (np.random.random(outX.shape) - 0.5) * prndStd
            outX -= trnd_mean
            outX /= 1.0 + trnd_std
            outX[outX < -1.0] = -1.0
            outX[outX > +1.0] = +1.0
        print ('-')
        yield (outX, outY)

if __name__ == '__main__':
    # fcsvTrn = '/Users/alexanderkalinovsky/data/@Face_Aging/01_UTKFace/UTKFace-x128-test-idx.txt'
    # fcsvVal = '/Users/alexanderkalinovsky/data/@Face_Aging/01_UTKFace/UTKFace-x128-test-idx.txt'
    #
    fcsvTrn = '/home/ar/data/@Face-Aging-Datasets/01_UTK_Faces/UTKFace-x128-idx.txt-train.txt'
    fcsvVal = '/home/ar/data/@Face-Aging-Datasets/01_UTK_Faces/UTKFace-x128-idx.txt-val.txt'
    #
    pkey = 'race'
    numEpochs = 100
    batchSize = 256
    pathModel = 'test_cnn_model_{0}.h5'.format(pkey)
    pathLog = '%s-log.csv' % pathModel
    #
    x_trn, y_trn, _ = readCSV(fcsvTrn, key=pkey)
    x_val, y_val, _ = readCSV(fcsvVal, key=pkey)
    input_shape = x_trn[0].shape
    num_classes = y_trn.shape[-1]
    numTrain = len(x_trn)
    #
    q = buildModel(pinp_shape = input_shape, numCls=num_classes)
    q.summary()
    #
    if not os.path.isfile(pathModel):
        model = buildModel(pinp_shape=input_shape, numCls=num_classes)
        model.compile(optimizer='adam',
                      loss='categorical_crossentropy',
                      metrics=['accuracy'])
    else:
        pathModelBk = '%s-%s.bk' % (pathModel, time.strftime('%Y.%m.%d-%H.%M.%S'))
        shutil.copy(pathModel, pathModelBk)
        model = keras.models.load_model(pathModel)
    model.summary()
    #
    model.fit_generator(
        # generator=data_generator.flow(x_train, y_train, batch_size=batchSize),
        generator=train_generator(x_trn, y_trn, batchSize=batchSize, isRandomize=True),
        epochs=numEpochs,
        steps_per_epoch=numTrain / batchSize,
        validation_data=(x_val, y_val),
        callbacks=[
            kall.ModelCheckpoint(pathModel, verbose=True, save_best_only=True, monitor='val_loss'),
            kall.CSVLogger(pathLog, append=True)
        ])
    print ('---')