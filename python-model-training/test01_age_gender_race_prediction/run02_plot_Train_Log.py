#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ar'

import os
import sys
import matplotlib.pyplot as plt
import pandas as pd
import time
import glob
import skimage.io as skio

if __name__ == '__main__':
    # pathModel = 'test_cnn_model_v1.h5'
    # pkey = 'gender'
    pkey = 'age'
    pathModel = 'test_cnn_model_{0}.h5'.format(pkey)
    flog = '%s-log.csv' % pathModel
    if os.path.isfile(flog):
        # data = pd.read_csv(flog, header=None).as_matrix()
        cnt = 0
        while True:
            data = pd.read_csv(flog)
            # dataIter = data['iter'].as_matrix()
            dataLossTrn = data['loss'].as_matrix()
            dataLossVal = data['val_loss'].as_matrix()
            dataAccTrn = data['acc'].as_matrix()
            dataAccVal = data['val_acc'].as_matrix()
            plt.clf()
            plt.subplot(1, 2, 1)
            plt.plot(dataLossTrn)
            plt.plot(dataLossVal)
            plt.grid(True)
            plt.legend(['loss-train', 'loss-validation'])
            plt.subplot(1, 2, 2)
            plt.plot(dataAccTrn)
            plt.plot(dataAccVal)
            plt.grid(True)
            plt.legend(['acc-train', 'acc-validation'], loc='best')
            #
            plt.show(block=False)
            plt.pause(5)
            print (':: update: [{0}]'.format(cnt))
            cnt += 1
    else:
        print ('*** WARNING *** cant find log-file [{0}]'.format(flog))