#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ar'

import os
import sys
import matplotlib.pyplot as plt
import pandas as pd
import time
import glob
import numpy as np
import skimage.io as skio
import skimage.transform as sktf
import keras.models

from run01_train_cnn_predictor_v1 import readCSV

def cropMaxSquare(pimg):
    nrow, ncol = pimg.shape[:2]
    if nrow<ncol:
        tret = pimg[:, (ncol - nrow) / 2:(ncol + nrow) / 2].copy()
    else:
        tret = pimg[(nrow - ncol) / 2:(ncol + nrow) / 2, :].copy()
    return tret

if __name__ == '__main__':
    pkey = 'gender'
    tmplModel = 'test_cnn_model_{0}.h5'
    pathModel = tmplModel.format(pkey)
    flog = '%s-log.csv' % pathModel
    # fcsvVal = '/home/ar/data/@Face-Aging-Datasets/01_UTK_Faces/UTKFace-x128-idx.txt-val.txt'
    # fcsvVal = '/Users/alexanderkalinovsky/data/@Face_Aging/01_UTKFace/UTKFace-x128-test-idx.txt'
    fcsvTest = '/Users/alexanderkalinovsky/data/BanubaVideoAll/BanubaVideo_Images_v2_faces/_tmp_out_good/idx.txt'

    _, _, imgPath = readCSV(fcsvTest)
    numImg = len(imgPath)
    lstImgx128 = []
    arrImg = None
    for ipath, path in enumerate(imgPath):
        timg = skio.imread(path)
        timgCrop = cropMaxSquare(timg)
        timg128 = sktf.resize(timgCrop, (128,128), preserve_range=True)
        print ('{0} : {1}'.format(path, timgCrop.shape))
        lstImgx128.append(timg128.astype(np.uint8))
        if arrImg is None:
            arrImg = np.zeros([numImg] + list(timg128.shape))
        timg128p = timg128.astype(np.float32)/127.5 - 1.
        arrImg[ipath] = timg128p

    lstKey = ['gender', 'age', 'race']
    # lstKey = ['gender']
    dictModel = dict()
    dictResults = dict()
    for kk in lstKey:
        fmodel = tmplModel.format(kk)
        tmodel = keras.models.load_model(fmodel)
        print ('----> [{0}]'.format(kk))
        tmodel.summary()
        dictModel[kk] = tmodel
        dictResults[kk] = tmodel.predict(arrImg)

    dictMaxIdx = dict()
    for kk,vv in dictResults.items():
        dictMaxIdx[kk] = np.argsort(-dictResults[kk])

    nx = 6
    ny = 5
    plt.figure()
    for iimg, img in enumerate(lstImgx128):
        tstr = ''
        for kk,vv in dictMaxIdx.items():
            tkk = kk[0]
            tvv = vv[iimg][0]
            if tkk == 'a':
                tvv = '[{0}-{1}]'.format(tvv * 10, (tvv+1)*10)
            elif tkk == 'g':
                tvv = ['m', 'f'][tvv]
            tstr += '{0} : {1}, '.format(tkk, tvv)
        plt.subplot(nx, ny, iimg + 1)
        plt.imshow(lstImgx128[iimg])
        plt.axis('off')
        plt.title(tstr)
        # print ('-')
    plt.show()

    print ('-')
