#!/bin/bash

tdir="/Users/alexanderkalinovsky/Workspace/tensorflow-1.0.0-sdk/bazel-bin/tensorflow/python/tools"

tf_freeze="${tdir}/freeze_graph"

${tdir}/freeze_graph --input_graph=$PWD/graph.pb --input_checkpoint=$PWD/model.ckpt --output_node_names='div' --output_graph=$PWD/frozen.pb
