#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ar'

import os
import glob
import sys
import keras
import keras.models
import shutil
import json
import tensorflow as tf

if __name__ == '__main__':
    wdir = '/Users/alexanderkalinovsky/dev-git.git/dev.python/PRJ_BANUBA/Step14_Face_Age_Gender_Race_Prediction/test01_age_gender_race_prediction/s02_convert_model/model_dir'
    tdir = '/Users/alexanderkalinovsky/Workspace/tensorflow-1.0.0-sdk/bazel-bin/tensorflow/python/tools'
    qdir = ''
    # (1)
    if len(sys.argv)>1:
        wdir = sys.argv[1]
    if not os.path.isdir(wdir):
        raise Exception(':: Cant find input directory with models: [{0}]'.format(wdir))
    # (2)
    if len(sys.argv)>2:
        tdir = sys.argv[2]
    if not os.path.isdir(tdir):
        raise Exception(':: Cant find directory with Tensorflow tools: [{0}]'.format(tdir))
    #
    exe_tf_graph_freeze = '{0}/freeze_graph'.format(tdir)
    exe_tf_graph_opt_inference = '{0}/optimize_for_inference'.format(tdir)
    if not os.path.isfile(exe_tf_graph_freeze):
        raise Exception(':: Cant find EXE: Freeze-Graph: [{0}]'.format(exe_tf_graph_freeze))
    if not os.path.isfile(exe_tf_graph_opt_inference):
        raise Exception(':: Cant find EXE: Opt-For-Inference: [{0}]'.format(exe_tf_graph_opt_inference))
    #
    lstModels = glob.glob('{0}/*.h5'.format(wdir))
    numModels = len(lstModels)
    for ipathModel, pathModel in enumerate(lstModels):
        with tf.Session() as sess:
            print (':: load model -> [{0}]'.format(pathModel))
            pathModel_ckpt = '{0}-graph.ckpt'.format(pathModel)
            pathModel_pb = '{0}-graph.pb'.format(pathModel)
            pathModel_cfg = '{0}-final.json'.format(pathModel)
            pathModel_cfg_q8 = '{0}-final-q8.json'.format(pathModel)
            #
            pathModeFreeze = '{0}-freeze.pb'.format(pathModel)
            pathModelOptInf = '{0}-final.pb'.format(pathModel)
            pathModelOptInfQ8 = '{0}-final-q8.pb'.format(pathModel)
            #
            model = keras.models.load_model(pathModel)
            model.summary()
            saver = tf.train.Saver()
            # saver.save(sess, "model.ckpt")
            saver.save(sess, pathModel_ckpt)
            # tf.train.write_graph(sess.graph_def, '', 'graph.pb')
            print ('\t:: write model Keras2tf graph -> [{0}]'.format(pathModel_pb))
            tf.train.write_graph(sess.graph_def, '', pathModel_pb)
            #

            lstInp = [xx.name.split(':')[0] for xx in model.inputs]
            lstOut = [xx.name.split(':')[0] for xx in model.outputs]

            lstInpShape = [ list(xx.output_shape[1:]) for xx in model.input_layers ]
            lstOutShape = [ list(xx.output_shape[1:]) for xx in model.output_layers]

            jsonCfg = {
                'inps': [],
                'outs': []
            }
            # (1) Input
            for iname, name in enumerate(lstInp):
                jsonCfg['inps'].append({
                    'name': lstInp[iname],
                    'shape': lstInpShape[iname]
                })
            # (2) Output
            for iname, name in enumerate(lstOut):
                jsonCfg['outs'].append({
                    'name': lstOut[iname],
                    'shape': lstOutShape[iname]
                })
            print ('\t:: save model Input/Output config in Json: [{0}]'.format(jsonCfg))
            with open(pathModel_cfg, 'w') as f:
                f.write(json.dumps(jsonCfg, indent=4))
            # modelInputName = model.input.name.split(':')[0]
            # modelOutputName = model.output.name.split(':')[0]
            print (json.dumps(jsonCfg, indent=4))
            # print ('Input: %s, Output: %s' % (modelInputName, modelOutputName))
            print ('\t\t---------')
            strOuts = ','.join(lstOut)
            strInps = ','.join(lstInp)
            cmd_freeze = "{0} --input_graph={1} --input_checkpoint={2} --output_node_names={3} --output_graph={4}"\
                .format(exe_tf_graph_freeze, pathModel_pb, pathModel_ckpt, strOuts, pathModeFreeze)
            cmd_final  = "{0} --input={1} --output={2} --output_names='{3}' --frozen_graph=True --input_names='{4}'"\
                .format(exe_tf_graph_opt_inference, pathModeFreeze, pathModelOptInf, strOuts, strInps)
            #
            print ('\t:: **Freeze** graph -> [{0}]'.format(pathModeFreeze))
            os.system(cmd_freeze)
            if not os.path.isfile(pathModeFreeze):
                raise Exception('!! Cant find freezed graph!! [{0}]'.format(pathModeFreeze))
            print ('\t:: **Optimize For Inference** graph -> [{0}]'.format(pathModelOptInf))
            os.system(cmd_final)
            if not os.path.isfile(pathModeFreeze):
                raise Exception('!! Cant find final (optimized for inference) graph!! [{0}]'.format(pathModelOptInf))
            else:
                print (' [Ok] graph is finalized! -> [{0}]'.format(pathModelOptInf))
                print ('\t Inputs -> [{0}], Outputs -> [{1}]'.format(strInps, strOuts))

