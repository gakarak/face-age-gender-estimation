#!/bin/bash

tdir="/Users/alexanderkalinovsky/Workspace/tensorflow-1.0.0-sdk/bazel-bin/tensorflow/python/tools"


${tdir}/optimize_for_inference --input=$PWD/frozen.pb --output=$PWD/final.pb --output_names='div' --frozen_graph=True --input_names=x
