#!/bin/bash

wdir="model_dir"

tdir="/Users/alexanderkalinovsky/Workspace/tensorflow-1.0.0-sdk/bazel-bin/tensorflow/python/tools"

tf_freeze="${tdir}/freeze_graph"


##${tdir}/freeze_graph --input_graph=$tdir/graph.pb --input_checkpoint=$PWD/model.ckpt --output_node_names='div' --output_graph=$PWD/frozen.pb


${tdir}/freeze_graph --input_graph=$wdir/test_cnn_model_mout_gender-age-race.h5-tf.pb \
    --input_checkpoint=$wdir/test_cnn_model_mout_gender-age-race.h5-tf.ckpt \
    --output_node_names='age/Softmax,gender/Softmax,race/Softmax' \
    --output_graph=$wdir/frozen.pb

${tdir}/optimize_for_inference --input=$wdir/frozen.pb \
    --output=$wdir/final.pb \
    --output_names='age/Softmax,gender/Softmax,race/Softmax' \
    --frozen_graph=True --input_names='input_1'
