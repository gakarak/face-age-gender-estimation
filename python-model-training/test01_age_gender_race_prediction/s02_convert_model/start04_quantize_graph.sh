#!/bin/bash

tdir="/Users/alexanderkalinovsky/Workspace/tensorflow-1.0.0-sdk/bazel-bin/tensorflow/python/tools"
bdir="/Users/alexanderkalinovsky/Workspace/tensorflow-1.0.0-sdk/bazel-bin"

${bdir}/tensorflow/tools/quantization/quantize_graph \
    --input=model_dir/test_cnn_model_mout_gender-age-race.h5-final.pb \
    --output_node_names='gender/Softmax,age/Softmax,race/Softmax' \
    --output=model_dir/q8-final.pb \
    --mode=eightbit
