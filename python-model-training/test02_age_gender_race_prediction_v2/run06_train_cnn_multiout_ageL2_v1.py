#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'ar'

import os
import time
import shutil
import matplotlib.pyplot as plt
import skimage.io as skio
import skimage.color as skcol
import numpy as np
import keras
from keras.layers import Conv2D, UpSampling2D, \
    Flatten, Activation, Reshape, MaxPooling2D, Input, Dense
from keras.models import Model
import keras.losses
import keras.callbacks as kall
import pandas as pd
import keras.preprocessing.image

from keras.utils.vis_utils import plot_model as kplot
from keras.utils import np_utils
from keras.utils.vis_utils import plot_model

# def readCSV(pcsv, keys=('gender', 'age', 'race'), asOneHot = False):
def readCSV(pcsv, keys, asOneHot = False):
    wdir = os.path.dirname(pcsv)
    data = pd.read_csv(pcsv)
    tpath = data['path'].as_matrix()
    retPath = np.array(['{0}/{1}'.format(wdir, xx) for xx in tpath])
    numPath = len(retPath)
    numKeys = len(keys)
    if asOneHot:
        retLbl = dict()
        for kk, vv in keys.items():
            tlbl = data[kk].as_matrix()
            if vv['act'] == 'softmax':
                tnumCls = vv['ncls']
                tlbl[tlbl>(tnumCls-1)] = tnumCls-1
                tlbl = np_utils.to_categorical(tlbl, tnumCls)
                retLbl[kk] = tlbl
            else:
                retLbl[kk] = tlbl
    else:
        retLbl = np.zeros((numPath, numKeys))
        for ikey, (key, vv) in enumerate(keys.items()):
            tlbl = data[key].as_matrix()
            if vv['act'] == 'softmax':
                tnumCls = vv['ncls']
                tlbl[tlbl > (tnumCls - 1)] = tnumCls - 1
                retLbl[:, ikey] = tlbl
            else:
                retLbl[:, ikey] = tlbl
    #
    retImg = []
    print (':: Loading images from [{0}]'.format(pcsv))
    numImg = len(retPath)
    for ii, pp in enumerate(retPath):
        timg = skio.imread(pp).astype(np.float32)/127.5 - 1.
        if timg.ndim<3:
            timg = skcol.gray2rgb(timg)
        retImg.append(timg)
        if (ii%100)==0:
            print ('\t[{0}/{1}] ...'.format(ii, numImg))
    #
    retImg = np.array(retImg)
    return (retImg, retLbl, retPath)

def buildModel(pinp_shape=(128,128,3), kernelSize = 3, numFlt = 4, numConv = 5, pkeys = {'gender':2, 'age': 10, 'race': 4}, numHidden=None, ppad='same'):
    dataInput = Input(shape=pinp_shape)
    x = dataInput
    # Conv #1
    for ii in range(numConv):
        if ii<4:
            tpad = ppad
        else:
            tpad = 'same'
        x = Conv2D(filters=numFlt * (2 ** ii), kernel_size=(kernelSize, kernelSize),
                       padding=tpad, activation='relu')(x)
        x = Conv2D(filters=numFlt * (2 ** ii), kernel_size=(kernelSize, kernelSize),
                   padding='same', activation='relu')(x)
        x = MaxPooling2D(pool_size=(2, 2))(x)
    #
    x = Flatten()(x)
    x_outputs = []
    for kk, vv in pkeys.items():
        if numHidden is not None:
            tx = Dense(units=numHidden, activation='relu')(x)
        else:
            tx = x
        tx = Dense(units=vv, activation='softmax', name=kk)(tx)
        x_outputs.append(tx)
    #
    model = Model(inputs=dataInput, outputs=x_outputs)
    return model

def buildModelL2(pinp_shape=(128,128,3), kernelSize = 3, numFlt = 4, numConv = 5, pkeys = None, numHidden=None, ppad='same'):
    dataInput = Input(shape=pinp_shape)
    x = dataInput
    # Conv #1
    for ii in range(numConv):
        if ii<4:
            tpad = ppad
        else:
            tpad = 'same'
        x = Conv2D(filters=numFlt * (2 ** ii), kernel_size=(kernelSize, kernelSize),
                       padding=tpad, activation='relu')(x)
        x = Conv2D(filters=numFlt * (2 ** ii), kernel_size=(kernelSize, kernelSize),
                   padding='same', activation='relu')(x)
        x = MaxPooling2D(pool_size=(2, 2))(x)
    #
    x = Flatten()(x)
    x_outputs = []
    # x_outputs = dict()
    for kk, vv in pkeys.items():
        if numHidden is not None:
            tx = Dense(units=numHidden, activation='relu')(x)
        else:
            tx = x
        fact = vv['act']
        if fact == 'softmax':
            tx = Dense(units=vv['ncls'], activation=fact, name=kk)(tx)
        else:
            tx = Dense(units=1, activation=fact, name=kk)(tx)
        x_outputs.append(tx)
        # x_outputs[kk] = tx
    #
    model = Model(inputs=dataInput, outputs=x_outputs)
    return model

def train_generator(dataX, dataY, pkeys, batchSize=256, prndShift = 0.15, prndStd = 0.1, isRandomize = True):
    data_generator = keras.preprocessing.image.ImageDataGenerator(
        rotation_range=10,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True
    )
    while True:
        outX, _outY = next(data_generator.flow(dataX, dataY, batch_size=batchSize))
        outY = dict()
        for ii, (kk,vv) in enumerate(pkeys.items()):
            if vv['act'] == 'softmax':
                tnumCls = vv['ncls']
                outY[kk] = np_utils.to_categorical(_outY[:,ii], tnumCls)
            else:
                outY[kk] = _outY[:,ii]
        if isRandomize:
            trnd_mean = (np.random.random(outX.shape) - 0.5) * prndShift
            trnd_std  = (np.random.random(outX.shape) - 0.5) * prndStd
            outX -= trnd_mean
            outX /= 1.0 + trnd_std
            outX[outX < -1.0] = -1.0
            outX[outX > +1.0] = +1.0
            # print ('-')
        yield (outX, outY)

if __name__ == '__main__':
    # fcsvTrn = '/Users/alexanderkalinovsky/data/@Face_Aging/01_UTKFace/UTKFace-x128-test-idx.txt'
    # fcsvVal = '/Users/alexanderkalinovsky/data/@Face_Aging/01_UTKFace/UTKFace-x128-test-idx.txt'
    #
    fcsvTrn = '/home/ar/data/@Face-Aging-Datasets/01_UTK_Faces/UTKFace-x128-idx3.txt-train.txt'
    fcsvVal = '/home/ar/data/@Face-Aging-Datasets/01_UTK_Faces/UTKFace-x128-idx3.txt-val.txt'
    # fcsvTrn = '/Users/alexanderkalinovsky/data/@Face_Aging/01_UTKFace/UTKFace-x128-test-idx.txt'
    # fcsvVal = fcsvTrn
    #
    # pkeys = ['gender','age', 'race']
    pkeys = {
        'gender':   {
            'loss': 'categorical_crossentropy',
            'ncls': 2,
            'act': 'softmax'
        },
        'age': {
            'loss': 'mean_squared_error',
            'ncls': -1,
            'act': 'linear'
        }
        # 'age':      16,
        # 'race':     5
    }
    numEpochs = 150
    batchSize = 256
    pathModelRestart = 'restart_model.h5'
    pathModel = 'cnn_model_ga5_mout_{0}.h5'.format('-'.join(pkeys.keys()))
    pathModelSave = 'mout_ga5_{epoch:03d}_{val_loss:0.3f}.h5'
    pathLog = '%s-log.csv' % pathModel
    #
    x_trn, y_trn, _ = readCSV(fcsvTrn, keys=pkeys, asOneHot=False)
    x_val, y_val, _ = readCSV(fcsvVal, keys=pkeys, asOneHot=True)
    input_shape = x_trn[0].shape
    numTrain = len(x_trn)
    numIterPerEpoch = numTrain / batchSize
    if numIterPerEpoch < 1:
        numIterPerEpoch = 1
    #
    # q = buildModel(pinp_shape = input_shape, numOutputs=pkeys)
    # q.summary()
    #
    if not os.path.isfile(pathModelRestart):
        model = buildModelL2(pinp_shape=input_shape, pkeys=pkeys, numFlt=6, ppad='valid')
        model.compile(optimizer='adam',
                      loss={xx: vv['loss'] for xx,vv in pkeys.items()},
                      metrics=['accuracy'])
    else:
        # pathModelBk = '%s-%s.bk' % (pathModel, time.strftime('%Y.%m.%d-%H.%M.%S'))
        # shutil.copy(pathModel, pathModelBk)
        model = keras.models.load_model(pathModelRestart)
    model.summary()
    #
    model.fit_generator(
        # generator=data_generator.flow(x_train, y_train, batch_size=batchSize),
        generator=train_generator(x_trn, y_trn, batchSize=batchSize, isRandomize=True, pkeys = pkeys),
        epochs=numEpochs,
        steps_per_epoch=numIterPerEpoch,
        validation_data=(x_val, y_val),
        callbacks=[
            kall.ModelCheckpoint(pathModelSave, verbose=True, save_best_only=False, monitor='val_loss'),
            kall.CSVLogger(pathLog, append=True)
        ])
    print ('---')